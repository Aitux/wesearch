package fil.glp.wesearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WesearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(WesearchApplication.class, args);
	}

}
