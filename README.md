# README #

Pour l'instant, ce README sert à laisser des notes aux autres développeurs. Dans le futur il devra être documenté sur comment configurer, installer et lancer l'application.

### What is this repository for? ###

* Ce repo (repository) stock le frontend et le backend.

### How do I get set up? ###

* Il est nécessaire pour pouvoir travailler sur le projet d'avoir au minimum
* * NodeJS et NPM d'installés
* * Java d'installé
* Il est obligatoire d'utiliser les environnements de développement IntelliJ Idea pour le backend et Webstorm pour le frontend. le gitignore ne prend pas en compte les autres IDE et cela pourrait causer des bugs dans le projet d'utiliser un autre IDE !!!

### Who do I talk to? ### 
* Autres contacts: Simon, Océane, Yann, Alexandre.