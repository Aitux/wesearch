
Projets les plus commentés 

select p.title, count(*) from project p
JOIN commentable c on p.idcommentable = c.idcommentable
JOIN isabout i on c.idcommentable = i.idcommentable
group by p.title;
-----------------------------------------------

----------------------------------------------------------
Durée des projet en cours (date actuelle - date de début) => permet de voir les projets les plus longs en cours 

select description, ([NOW_DATE]- startingDate) as duree from project
group by description, startingDate
having  ([NOW_DATE]- startingDate) > 0
ORDER BY ([NOW_DATE]- startingDate) DESC ;

-----------------------------------------

Archivage des projets (date de fin - date de début)

---------------------------------------------
Nombre de projets par domaine
select d.libelle, count(*) from project p
JOIN domain d on p.iddomain = d.iddomain
group by d.libelle;


-------------------------------------------
Nombre d'utilisateurs par projets 

select p.description, count(*) from users u
JOIN project p on u.idusers = p.idusers
group by p.description;
----------------------------------

Nombre moyen de domaines par projet 
------------------------------------
select avg(idproject) from (select DISTINCT idproject, iddomain from project ORDER BY idproject asc) as foo;

Nombre de domaines par projets
------------------------------------
select idproject, count(*) 
from 
(select DISTINCT idproject, iddomain from project ORDER BY idproject asc) as foo GROUP BY idproject;


Nombre de commentaires par projets => On part juste de isAbout, on compte en fonction des idCommenntable, on joint ensuite avec porject pour avoir les noms des projets
-----------------------------------
select count(*), i.idcommentable, p.description
from isabout i
join project p on i.idcommentable = p.idcommentable
GROUP BY i.idcommentable, p.description ;
------------------------------------------------

Nombre de posts par utilisateurs
----------------------------------------------------
select u.nom, count(*) as nbr from users u
JOIN post p on u.idusers = p.idusers
group by u.nom
ORDER BY nbr desc;
------------------------------------------------
Nombre d'utilisateurs par laboratoire => OK
Nombre de commentaires par utilisateurs => OK
---------------------------------------------------

Nombre moyen de commentaires par utilisateurs par jour 
Elements les plus cités (projets, utilisateurs, domaine, laboratoire...)
Nombre moyen de posts par utilisateurs par jour  
Elements les plus recherchés (barre de recherche)
Nombre moyen d'inscriptions par jour (nouveau user par jour) => nécessite une date d'inscription
Nombre moyen de commentaires par jour sur les projets
 
