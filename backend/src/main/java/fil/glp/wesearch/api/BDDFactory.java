package fil.glp.wesearch.api;

import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.postgresql.ds.PGSimpleDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * The type Bdd factory.
 */
public class BDDFactory {
    private final static Logger logger = LoggerFactory.getLogger(BDDFactory.class);
    private static Jdbi jdbi = null;

    /**
     * Instantiates a new Bdd factory.
     */
    public BDDFactory() {
    }

    private static Jdbi getJdbi() {
        if (jdbi == null) {
            String username = System.getenv("SPRING_DATASOURCE_USERNAME");
            String password = System.getenv("SPRING_DATASOURCE_PASSWORD");
            String dbUrl = System.getenv("SPRING_DATASOURCE_URL");
            PGSimpleDataSource basicDataSource = new PGSimpleDataSource();
            basicDataSource.setUrl(dbUrl);
            basicDataSource.setUser(username);
            basicDataSource.setPassword(password);
            jdbi = Jdbi.create(basicDataSource);
            jdbi.installPlugin(new SqlObjectPlugin());
        }
        return jdbi;
    }

    /**
     * Table exist boolean.
     *
     * @param tableName the table name
     * @return the boolean
     * @throws SQLException the sql exception
     */
    static boolean tableExist(String tableName) throws SQLException {
        DatabaseMetaData dbm = getJdbi().open().getConnection().getMetaData();
        ResultSet tables = dbm.getTables(null, null, tableName, null);
        boolean exist = tables.next();
        tables.close();
        return exist;
    }

    /**
     * Build dao t.
     *
     * @param <T>      the type parameter
     * @param daoClass the dao class
     * @return the t
     */
    public static <T> T buildDao(Class<T> daoClass) {
        return getJdbi().onDemand(daoClass);
    }
}
