package fil.glp.wesearch.web.dto;

public class MentionnedDTO {

    private int idMentionned;
    private int idUsers;
    private int idpost;

    public int getIdMentionned() {
        return idMentionned;
    }

    public void setIdMentionned(int idMentionned) {
        this.idMentionned = idMentionned;
    }

    public int getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(int idUsers) {
        this.idUsers = idUsers;
    }

    public int getIdpost() {
        return idpost;
    }

    public void setIdpost(int idpost) {
        this.idpost = idpost;
    }
}
