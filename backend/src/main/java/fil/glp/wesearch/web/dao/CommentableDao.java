package fil.glp.wesearch.web.dao;

import fil.glp.wesearch.web.model.Commentable;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface CommentableDao {

    @SqlQuery("SELECT * FROM commentable WHERE idcommentable=:idcommentable")
    @RegisterBeanMapper(Commentable.class)
    Commentable findById(@Bind("idcommentable") int idcommentable);

    @SqlUpdate("insert into commentable (idcommentable) values (:idcommentable)")
    void insert(@BindBean() Commentable commentable);

    @SqlQuery("SELECT * FROM commentable")
    @RegisterBeanMapper(Commentable.class)
    List<Commentable> all();

    @SqlUpdate("delete from commentable where idcommentable = :idcommentable")
    void delete(@Bind("idcommentable") int idcommentable);

    @SqlUpdate("UPDATE * from commentable where idcommentable=:idcommentable")
    int update(@Bind("idcommentable") int idcommentable);
}
