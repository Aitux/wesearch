package fil.glp.wesearch.web.model;

import fil.glp.wesearch.web.controller.ProjectController;
import fil.glp.wesearch.web.controller.UserController;
import fil.glp.wesearch.web.dto.ProjectDTO;
import fil.glp.wesearch.web.dto.UsersDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ResponseQueryLF {

    private List<ProjectDTO> projects;
    private List<UsersDTO> users;

    public ResponseQueryLF() {
        projects = new ArrayList<>();
        users = new ArrayList<>();
    }

    public ResponseQueryLF(List<ProjectDTO> projects, List<UsersDTO> users) {
        this.projects = projects;
        this.users = users;
    }

    public static ResponseQueryLF parseList(List<LookFor> qlf) {
        UserController uc = new UserController();
        ProjectController pc = new ProjectController();
        ResponseQueryLF rqlf = new ResponseQueryLF();
        for (LookFor l : qlf) {
            if (Objects.isNull(l.getIdproject())) {
                rqlf.users.add(uc.findById(l.getIdusers()));
            } else if (Objects.isNull(l.getIdusers())) {
                rqlf.projects.add(pc.findById(l.getIdproject()));
            }
        }
        return rqlf;
    }

    public List<ProjectDTO> getProjects() {
        return projects;
    }

    public void setProjects(List<ProjectDTO> projects) {
        this.projects = projects;
    }

    public List<UsersDTO> getUsers() {
        return users;
    }

    public void setUsers(List<UsersDTO> users) {
        this.users = users;
    }
}
