package fil.glp.wesearch.web.model;

public class Mentionned {
	private int idMentionned;
	private int idUsers;
	private int idpost;
	
	public Mentionned() {
		super();
	}

	public Mentionned(int idMentionned, int idUsers, int idpost) {
		super();
		this.idMentionned = idMentionned;
		this.idUsers = idUsers;
		this.idpost = idpost;
	}


	public int getIdUsers() {
		return idUsers;
	}

	public void setIdUsers(int idUsers) {
		this.idUsers = idUsers;
	}

	public int getIdpost() {
		return idpost;
	}

	public void setIdpost(int idpost) {
		this.idpost = idpost;
	}

	public int getIdMentionned() {
		return idMentionned;
	}

	public void setIdMentionned(int idMentionned) {
		this.idMentionned = idMentionned;
	}	

}
