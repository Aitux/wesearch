package fil.glp.wesearch.web.dao;

import fil.glp.wesearch.web.model.Study;
import fil.glp.wesearch.web.model.Tag;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface TagDao {

    @SqlQuery("SELECT * FROM tag WHERE idtag=:idtag")
    @RegisterBeanMapper(Tag.class)
    Tag findById(@Bind("idtag") int idtag);

    @SqlUpdate("insert into tag (idtag, libelle, field) values (:idtag, :libelle, :field )")
    void insert(@BindBean() Tag tag);

    @SqlQuery("SELECT * FROM tag")
    @RegisterBeanMapper(Tag.class)
    List<Tag> all();

    @SqlUpdate("delete from tag where idtag=:idtag")
    void delete(@Bind("idtag") int idtag);

    @SqlUpdate("UPDATE * from idtag where idtag=:idtag")
    int update(@Bind("idtag") int idtag);
}

