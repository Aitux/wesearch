package fil.glp.wesearch.web.dto;

public class IsaboutDTO {

    private int idCommentable;
    private int idcomment;

    public int getIdCommentable() {
        return idCommentable;
    }

    public void setIdCommentable(int idCommentable) {
        this.idCommentable = idCommentable;
    }

    public int getIdcomment() {
        return idcomment;
    }

    public void setIdcomment(int idcomment) {
        this.idcomment = idcomment;
    }
}
