package fil.glp.wesearch.web.dao;

import fil.glp.wesearch.web.model.Propose;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface ProposeDao {

    @SqlQuery("SELECT * FROM propose WHERE idproject=:idproject")
    @RegisterBeanMapper(Propose.class)
    List<Propose> findById(@Bind("idproject") int idproject);

    @SqlUpdate("insert into propose (idproject, label) values (:idproject, :label)")
    @GetGeneratedKeys
    int insert(@Bind("idproject") Integer idproject, @Bind("label") String label);

    @SqlQuery("SELECT * FROM propose")
    @RegisterBeanMapper(Propose.class)
    List<Propose> all();

    @SqlUpdate("delete from propose where idpropose=:idpropose")
    void delete(@Bind("idpropose") Integer idpropose);

}
