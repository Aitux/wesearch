package fil.glp.wesearch.web.dto;

import java.util.Date;

public class CommentDTO {

    private Integer idcomment;
    private Integer idUsers;
    private String content;
    private Date date;
    private Integer idcommentable;
    private UsersDTO users;

    public UsersDTO getUsers() {
        return users;
    }

    public void setUsers(UsersDTO users) {
        this.users = users;
    }

    public Integer getIdcomment() {
        return idcomment;
    }

    public void setIdcomment(Integer idcomment) {
        this.idcomment = idcomment;
    }

    public Integer getIdcommentable() {
        return idcommentable;
    }

    public void setIdcommentable(Integer idcommentable) {
        this.idcommentable = idcommentable;
    }

    public Integer getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(Integer idUsers) {
        this.idUsers = idUsers;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
