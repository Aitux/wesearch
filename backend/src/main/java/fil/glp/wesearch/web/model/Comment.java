package fil.glp.wesearch.web.model;

import fil.glp.wesearch.util.Util;
import fil.glp.wesearch.web.controller.UserController;
import fil.glp.wesearch.web.dto.CommentDTO;
import fil.glp.wesearch.web.dto.UsersDTO;

import java.util.Date;

public class Comment {
    private int idcomment;
    private int idUsers;
    private String content;
    private Date datecom;

    public Comment() {
        super();
    }



    public int getIdcomment() {
        return idcomment;
    }


    public void setIdcomment(int idcomment) {
        this.idcomment = idcomment;
    }


    public int getIdUsers() {
        return idUsers;
    }


    public void setIdUsers(int idUsers) {
        this.idUsers = idUsers;
    }


    public String getContent() {
        return content;
    }


    public void setContent(String content) {
        this.content = content;
    }


	public Date getDatecom() {
		return datecom;
	}

	public void setDatecom(Date datecom) {
		this.datecom = datecom;
	}

	public CommentDTO toDto() {
        UserController uc = new UserController();
        UsersDTO user = uc.findById(getIdUsers());
        CommentDTO dto = new CommentDTO();
        dto.setContent(Util.parseContent(content));
        dto.setDate(getDatecom());
        dto.setUsers(user);
        dto.setIdcomment(getIdcomment());
        return dto;
    }
}
