package fil.glp.wesearch.web.model;

public class Domain {

    private Integer iddomain;
    private String libelle;

    public Domain() {
        super();
    }

    public Domain(int iddomain, String libelle) {
        super();
        this.iddomain = iddomain;
        this.libelle = libelle;
    }

    public int getIddomain() {
        return iddomain;
    }

    public void setIddomain(int iddomain) {
        this.iddomain = iddomain;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }


}
