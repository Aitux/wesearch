package fil.glp.wesearch.web.dto;

import java.util.Date;

public class ProjectDTO {

    private Integer idcommentable;
    private Integer idproject;
    private UsersDTO creator;
    private String domain;
    private Integer idlab;
    private String title;
    private String description;
    private Date startingdate;

    public Integer getIdcommentable() {
        return idcommentable;
    }

    public void setIdcommentable(Integer idcommentable) {
        this.idcommentable = idcommentable;
    }

    public Integer getIdproject() {
        return idproject;
    }

    public void setIdproject(Integer idproject) {
        this.idproject = idproject;
    }

    public UsersDTO getCreator() {
        return creator;
    }

    public void setCreator(UsersDTO creator) {
        this.creator = creator;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Integer getIdlab() {
        return idlab;
    }

    public void setIdlab(Integer idlab) {
        this.idlab = idlab;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartingdate() {
        return startingdate;
    }

    public void setStartingdate(Date startingdate) {
        this.startingdate = startingdate;
    }

}
