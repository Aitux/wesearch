package fil.glp.wesearch.web.model;

public class Isabout {
    private int idCommentable;
    private int idcomment;

    public Isabout() {
        super();
    }


    public Isabout(int idCommentable, int idcomment) {
        super();
        this.idCommentable = idCommentable;
        this.idcomment = idcomment;
    }


    public int getIdCommentable() {
        return idCommentable;
    }


    public void setIdCommentable(int idCommentable) {
        this.idCommentable = idCommentable;
    }


    public int getIdcomment() {
        return idcomment;
    }


    public void setIdcomment(int idcomment) {
        this.idcomment = idcomment;
    }


}
