package fil.glp.wesearch.web.model;

import fil.glp.wesearch.util.Util;
import fil.glp.wesearch.web.controller.ProjectController;
import fil.glp.wesearch.web.controller.UserController;
import fil.glp.wesearch.web.dto.PostDTO;
import fil.glp.wesearch.web.dto.UsersDTO;
import fil.glp.wesearch.web.exception.ItemNotFoundException;

import java.util.Date;

public class Post {

    private Integer idcommentable;
    private Integer idpost;
    private String content;
    private Date datepost;
    private Integer idusers;

    public Post() {
    }

    public Post(Integer idcommentable, Integer idpost, String content, Date datepost, Integer idusers) {
        this.idcommentable = idcommentable;
        this.idpost = idpost;
        this.content = content;
        this.datepost = datepost;
        this.idusers = idusers;
    }

    public Integer getIdcommentable() {
        return idcommentable;
    }

    public void setIdcommentable(Integer idcommentable) {
        this.idcommentable = idcommentable;
    }

    public Integer getIdpost() {
        return idpost;
    }

    public void setIdpost(Integer idpost) {
        this.idpost = idpost;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDatepost() {
        return datepost;
    }

    public void setDatepost(Date datepost) {
        this.datepost = datepost;
    }

    public Integer getIdusers() {
        return idusers;
    }

    public void setIdusers(Integer idusers) {
        this.idusers = idusers;
    }

    public PostDTO toDto() {
        UserController uc = new UserController();
        UsersDTO user = uc.findById(getIdusers());
        System.out.println(user);
        PostDTO dto = new PostDTO();
        dto.setContent(Util.parseContent(content));
        dto.setDate(getDatepost());
        dto.setUsers(user);
        dto.setIdcommentable(getIdcommentable());
        dto.setIdPost(getIdpost());
        return dto;
    }



    public Integer compareTo(Post o) {
        return getDatepost().compareTo(o.getDatepost());
    }
}
