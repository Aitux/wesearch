package fil.glp.wesearch.web.dto;

public class DomainDTO {

    private Integer iddomain;
    private String libelle;

    public int getIddomain() {
        return iddomain;
    }

    public void setIddomain(int iddomain) {
        this.iddomain = iddomain;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
