package fil.glp.wesearch.web.controller;

import fil.glp.wesearch.api.BDDFactory;
import fil.glp.wesearch.web.dao.PostDao;
import fil.glp.wesearch.web.dto.PostDTO;
import fil.glp.wesearch.web.exception.ItemNotFoundException;
import fil.glp.wesearch.web.model.Post;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class PostController {

    PostDao dao = BDDFactory.buildDao(PostDao.class);

    public PostController() {
    }

    @PostMapping(value = "/post", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PostDTO insert(@RequestBody Post post) {
        dao.insert(post);
        return post.toDto();
    }

    /**
     * Delete.
     *
     * @param id the id
     * @throws ItemNotFoundException the item not found exception
     */
    @DeleteMapping(value = "/post/{id}")
    public void delete(@PathVariable("id") int id) throws ItemNotFoundException {
        try {
            deletePost(id);
        } catch (ItemNotFoundException e) {
            throw e;
        }
    }

    /**
     * Delete user.
     *
     * @param id the id
     */
    void deletePost(int id) {
        dao.delete(id);
    }

    @GetMapping(value = "/post", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<PostDTO> all(){
        return dao.all().stream().map(Post::toDto).collect(Collectors.toList());
    }


}

