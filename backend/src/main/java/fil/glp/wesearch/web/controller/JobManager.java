package fil.glp.wesearch.web.controller;

import fil.glp.wesearch.api.BDDFactory;
import fil.glp.wesearch.web.dao.JobDao;
import fil.glp.wesearch.web.model.Job;

public class JobManager {

    JobDao dao = BDDFactory.buildDao(JobDao.class);

    public Job findById(Integer id) {
        return dao.findById(id);
    }
}
