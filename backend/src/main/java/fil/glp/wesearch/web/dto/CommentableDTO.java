package fil.glp.wesearch.web.dto;

public class CommentableDTO {

    private int idcommentable;

    public int getIdcommentable() {
        return idcommentable;
    }

    public void setIdcommentable(int idcommentable) {
        this.idcommentable = idcommentable;
    }
}
