package fil.glp.wesearch.web.controller;

import fil.glp.wesearch.api.BDDFactory;
import fil.glp.wesearch.web.dao.TakePartDao;
import fil.glp.wesearch.web.dto.TakePartDTO;
import fil.glp.wesearch.web.model.Takepart;
import fil.glp.wesearch.web.security.BooleanResponse;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
public class TakePartController {

    TakePartDao dao = BDDFactory.buildDao(TakePartDao.class);

    @PostMapping(value = "/takepart", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public TakePartDTO insert(@RequestBody Takepart takePart) {
        dao.insert(takePart.getIdUsers(), takePart.getIdproject());
        return takePart.toDTO();
    }

    @GetMapping(value = "/takepart/{idusers}/{idproject}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BooleanResponse takePart(@PathVariable("idusers") Integer idusers, @PathVariable("idproject") Integer idproject) {
        return new BooleanResponse(!Objects.isNull(dao.takePart(idusers, idproject)));
    }

    @DeleteMapping(value = "/takepart")
    @ResponseBody
    public BooleanResponse leaveProject(@RequestBody Takepart takepart) {
        dao.delete(takepart.getIdUsers(), takepart.getIdproject());
        return new BooleanResponse(true);
    }
}
