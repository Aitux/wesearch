package fil.glp.wesearch.web.dao;

import fil.glp.wesearch.web.model.Isabout;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface IsaboutDao {

    @SqlQuery("SELECT * FROM isabout WHERE idcommentable=:idcommentable")
    @RegisterBeanMapper(Isabout.class)
    List<Isabout> findById(@Bind("idcommentable") Integer idcommentable);

    @SqlUpdate("insert into isabout (idcommentable, idcomment) values (:idCommentable, :idcomment)")
    void insert(@BindBean() Isabout isabout);

    @SqlQuery("SELECT * FROM isabout")
    @RegisterBeanMapper(Isabout.class)
    List<Isabout> all();

    @SqlUpdate("delete from isabout where idcommentable=:idcommentable AND idcomment=:idcomment")
    void delete(@Bind("idcommentable") Integer idcommentable, @Bind("idcomment") Integer idcomment);

}
