package fil.glp.wesearch.web.model;

public class Tag {
	private int idtag;
	private String libelle;
	private String field;
	
	
	public Tag() {
		super();
	}


	public Tag(int idtag, String libelle, String field) {
		super();
		this.idtag = idtag;
		this.libelle = libelle;
		this.field = field;
	}


	public int getIdtag() {
		return idtag;
	}


	public void setIdtag(int idtag) {
		this.idtag = idtag;
	}


	public String getLibelle() {
		return libelle;
	}


	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}


	public String getField() {
		return field;
	}


	public void setField(String field) {
		this.field = field;
	}

	


}
