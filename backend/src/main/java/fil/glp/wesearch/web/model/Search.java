package fil.glp.wesearch.web.model;

import fil.glp.wesearch.web.dto.SearchDTO;

public class Search {
    private Integer idproject;
    private Integer idsearch;
    private String label;

    public Search() {
        super();
    }

    public Integer getIdproject() {
        return idproject;
    }

    public void setIdproject(Integer idproject) {
        this.idproject = idproject;
    }

    public Integer getIdsearch() {
        return idsearch;
    }

    public void setIdsearch(Integer idsearch) {
        this.idsearch = idsearch;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public SearchDTO toDto(){
    	SearchDTO res=  new SearchDTO();
    	res.setIdproject(getIdproject());
    	res.setId(getIdsearch());
    	res.setLabel(getLabel());
    	return res;
	}
}
