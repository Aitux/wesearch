package fil.glp.wesearch.web.dto;

public class LaboratoryDTO {

    private int idlab;
    private String libelle;

    public int getIdlab() {
        return idlab;
    }

    public void setIdlab(int idlab) {
        this.idlab = idlab;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
