package fil.glp.wesearch.web.controller;

import fil.glp.wesearch.api.BDDFactory;
import fil.glp.wesearch.web.dao.ProjectDao;
import fil.glp.wesearch.web.dao.ProposeDao;
import fil.glp.wesearch.web.dao.SearchDao;
import fil.glp.wesearch.web.dao.TakePartDao;
import fil.glp.wesearch.web.dto.ProjectDTO;
import fil.glp.wesearch.web.dto.ProposeDTO;
import fil.glp.wesearch.web.dto.SearchDTO;
import fil.glp.wesearch.web.exception.ItemNotFoundException;
import fil.glp.wesearch.web.model.Project;
import fil.glp.wesearch.web.model.Propose;
import fil.glp.wesearch.web.model.Search;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
public class ProjectController {
    ProjectDao dao = BDDFactory.buildDao(ProjectDao.class);
    TakePartDao takePartDao = BDDFactory.buildDao(TakePartDao.class);
    SearchDao searchDao = BDDFactory.buildDao(SearchDao.class);
    ProposeDao proposeDao = BDDFactory.buildDao(ProposeDao.class);


    public ProjectController() {
    }

    @GetMapping(value = "/project", produces = "application/json")
    public List<Project> all() {
        return dao.all();
    }

    @GetMapping(value = "/project/{id}", produces = "application/json")
    public @ResponseBody
    ProjectDTO findById(@PathVariable("id") int id) throws ItemNotFoundException {
        Project p = dao.findById(id);
        if (Objects.isNull(p))
            throw new ItemNotFoundException();
        return p.toDto();
    }


    @PostMapping(value = "/project")
    public ProjectDTO insert(@RequestBody Project project) {
        project.setIddomain(1);
        project.setIdlab(1);
        return addProject(project);
    }

    private ProjectDTO addProject(Project p) {
        Project pro = dao.insert(p.getIdUsers(), p.getTitle(), p.getDescription(), p.getStartingdate());
        p.setIdcommentable(pro.getIdcommentable());
        p.setIdproject(pro.getIdproject());
        takePartDao.insert(p.getIdUsers(), p.getIdproject());
        return p.toDto();
    }


    @DeleteMapping(value = "/project/{idProject}")
    public void delete(@PathVariable("idProject") int idProject) throws ItemNotFoundException {
        try {
            deleteProject(idProject);
        } catch (ItemNotFoundException e) {
            throw e;
        }
    }

    void deleteProject(int id) {
        dao.delete(id);
    }

    @PutMapping(value = "/project")
    public void updateProject(@RequestBody Project project) throws ItemNotFoundException {
        dao.update(verifparam(project));
    }

    private Project verifparam(Project project) {
        Project newProject = project;
        if (Objects.isNull(project.getDescription())) {
            newProject.setDescription(null);
        }
        if (Objects.isNull(project.getTitle())) {
            newProject.setTitle(null);
        }
        return newProject;
    }

    @GetMapping(value = "/searching/{idproject}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<SearchDTO> listSearch(@PathVariable("idproject") Integer idproject){
        List<Search> searches = searchDao.findById(idproject);
        return searches.stream().map(Search::toDto).collect(Collectors.toList());
    }

    @GetMapping(value = "/propose/{idproject}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ProposeDTO> listPropose(@PathVariable("idproject") Integer idproject){
        List<Propose> proposes = proposeDao.findById(idproject);
        return proposes.stream().map(Propose::toDto).collect(Collectors.toList());
    }

    @PostMapping(value = "/searching", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SearchDTO insertSearch(@RequestBody Search search){
        search.setIdsearch(searchDao.insert(search.getIdproject(), search.getLabel()));
        return search.toDto();
    }

    @PostMapping(value = "/propose", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ProposeDTO insertPropose(@RequestBody Propose propose){
        propose.setIdpropose(proposeDao.insert(propose.getIdproject(), propose.getLabel()));
        return propose.toDto();
    }

    @DeleteMapping(value = "/searching/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteSearch(@PathVariable("id") Integer id){
        searchDao.delete(id);
    }

    @DeleteMapping(value = "/propose/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deletePropose(@PathVariable("id") Integer id){
        proposeDao.delete(id);
    }


}
