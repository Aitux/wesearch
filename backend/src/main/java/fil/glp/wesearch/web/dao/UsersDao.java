package fil.glp.wesearch.web.dao;

import fil.glp.wesearch.web.model.Users;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlBatch;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface UsersDao {

    @SqlQuery("SELECT * FROM users WHERE mail=:mail AND password=:password")
    @RegisterBeanMapper(Users.class)
    Users authorize(@Bind("mail") String mail, @Bind("password") String password);

    @SqlUpdate("insert into users (idlab, nom, prenom, mail, password, numero, idrole, idjob) values (:idlab, :nom, :prenom, :mail, :password, :numero, :idrole, :idjob)")
    void insert(@BindBean() Users users);

    @SqlQuery("SELECT * FROM users WHERE idusers=:idusers")
    @RegisterBeanMapper(Users.class)
    Users findById(@Bind("idusers") int idusers);

    @SqlQuery("SELECT * FROM users")
    @RegisterBeanMapper(Users.class)
    List<Users> all();

    @SqlUpdate("delete from Users where idusers = :idusers")
    void delete(@Bind("idusers") int idusers);

    @SqlUpdate("UPDATE users set idlab = :idlab, nom=:nom, prenom=:prenom, mail=:mail, numero=:numero, idjob=:idjob where idusers=:idusers")
    void update(@BindBean() Users users);

}
