package fil.glp.wesearch.web.model;

public class CountFollow {

    private Integer nbfollow;

    public CountFollow() {
    }

    public CountFollow(Integer nbfollow) {
        this.nbfollow = nbfollow;
    }

    public Integer getNbfollow() {
        return nbfollow;
    }

    public void setNbfollow(Integer nbfollow) {
        this.nbfollow = nbfollow;
    }
}
