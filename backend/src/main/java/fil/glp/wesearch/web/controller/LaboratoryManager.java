package fil.glp.wesearch.web.controller;

import fil.glp.wesearch.api.BDDFactory;
import fil.glp.wesearch.web.dao.LaboratoryDao;
import fil.glp.wesearch.web.model.Laboratory;

public class LaboratoryManager {


    LaboratoryDao dao = BDDFactory.buildDao(LaboratoryDao.class);

    public Laboratory findById(Integer id) {
        return dao.findById(id);
    }
}
