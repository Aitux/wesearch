package fil.glp.wesearch.web.dao;

import fil.glp.wesearch.web.model.CountFollow;
import fil.glp.wesearch.web.model.Follow;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface FollowDao {



    @SqlQuery("SELECT * from follow")
    @RegisterBeanMapper(Follow.class)
    List<Follow> all();

    @SqlUpdate("INSERT INTO follow (iduserfollow, iduserfollowed) values (:iduserfollow, :iduserfollowed)")
    void insert(@BindBean() Follow follow);

    @SqlQuery("SELECT * FROM follow where iduserfollow=:id")
    @RegisterBeanMapper(Follow.class)
    List<Follow> findFollower(@Bind("id") int id);

    @SqlQuery("SELECT * FROM follow where iduserfollowed=:id")
    @RegisterBeanMapper(Follow.class)
    List<Follow> findFollowed(@Bind("id") int id);

    @SqlQuery("SELECT count(*) as nbfollow from follow where iduserfollow=:iduserfollow")
    @RegisterBeanMapper(CountFollow.class)
    CountFollow countFollower(@Bind("iduserfollow") Integer iduserfollow);

    @SqlQuery("SELECT * from follow where iduserfollow=:iduserfollow AND iduserfollowed=:iduserfollowed")
    @RegisterBeanMapper(Follow.class)
    Follow isFollow(@Bind("iduserfollow") Integer iduserfollow, @Bind("iduserfollowed") Integer iduserfollowed);

    @SqlUpdate("DELETE FROM follow where iduserfollow=:iduserfollow AND iduserfollowed=:iduserfollowed")
    void delete(@Bind("iduserfollow") Integer iduserfollow, @Bind("iduserfollowed") Integer iduserfollowed);
}
