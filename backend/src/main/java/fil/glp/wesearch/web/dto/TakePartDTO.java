package fil.glp.wesearch.web.dto;

public class TakePartDTO {

    private int idUsers;
    private int idproject;


    public int getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(int idUsers) {
        this.idUsers = idUsers;
    }

    public int getIdproject() {
        return idproject;
    }

    public void setIdproject(int idproject) {
        this.idproject = idproject;
    }


}
