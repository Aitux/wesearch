package fil.glp.wesearch.web.dao;

import fil.glp.wesearch.web.model.Search;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

public interface SearchDao {

    @SqlQuery("SELECT * FROM search WHERE idproject=:idproject")
    @RegisterBeanMapper(Search.class)
    List<Search> findById(@Bind("idproject") int idproject);

    @SqlUpdate("insert into search (idproject, label) values (:idproject, :label)")
    @GetGeneratedKeys
    int insert(@Bind("idproject") Integer idproject, @Bind("label") String label);

    @SqlQuery("SELECT * FROM search")
    @RegisterBeanMapper(Search.class)
    List<Search> all();

    @SqlUpdate("delete from search where idsearch=:idsearch")
    void delete(@Bind("idsearch") Integer idsearch);
}
