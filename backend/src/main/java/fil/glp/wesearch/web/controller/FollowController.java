package fil.glp.wesearch.web.controller;

import com.sun.org.apache.xpath.internal.operations.Bool;
import fil.glp.wesearch.api.BDDFactory;
import fil.glp.wesearch.web.dao.FollowDao;
import fil.glp.wesearch.web.dto.FollowDTO;
import fil.glp.wesearch.web.model.CountFollow;
import fil.glp.wesearch.web.model.Follow;
import fil.glp.wesearch.web.security.BooleanResponse;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RestController
public class FollowController {

    FollowDao dao = BDDFactory.buildDao(FollowDao.class);

    public FollowController() {
    }

    @GetMapping(value = "/follow")
    @ResponseBody
    public List<FollowDTO> all() {
        List<Follow> followList = dao.all();
        List<FollowDTO> dto = new ArrayList<>();
        for (Follow f : followList) {
            FollowDTO d = f.toDTO();
            dto.add(d);
        }
        return dto;
    }

    @PostMapping(value = "/follow", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public FollowDTO insert(@RequestBody Follow follow) {
        dao.insert(follow);
        return follow.toDTO();
    }

    @GetMapping(value = "/follow/{idfo}/{idfoed}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BooleanResponse isFollow(@PathVariable("idfo") Integer idfo, @PathVariable("idfoed") Integer idfoed){
        return new BooleanResponse(!Objects.isNull(dao.isFollow(idfo, idfoed)));
    }

    @DeleteMapping(value = "/follow")
    @ResponseBody
    public BooleanResponse unfollow(@RequestBody Follow follow){
        dao.delete(follow.getIduserfollow(), follow.getIduserfollowed());
        return new BooleanResponse(true);
    }

    @GetMapping(value = "/follow/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    CountFollow countFollow(@PathVariable("id") int id){
        return dao.countFollower(id);
    }


}
