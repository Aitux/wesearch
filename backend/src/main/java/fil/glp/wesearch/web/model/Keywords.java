package fil.glp.wesearch.web.model;

import java.util.ArrayList;
import java.util.List;

public class Keywords {
    public List<Character> KEYWORDS = new ArrayList<>();

    private static Keywords instance;

    private Keywords(){
        KEYWORDS.add('@');
        KEYWORDS.add('!');
        KEYWORDS.add('#');
    }

    public static Keywords getInstance() {
        if(instance == null)
            instance = new Keywords();
        return instance;
    }
}
