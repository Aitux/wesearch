package fil.glp.wesearch.web.dao;

import fil.glp.wesearch.web.model.LookFor;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

import java.util.List;

public interface LookForDao {

    @SqlQuery("SELECT idusers as idusers, NULL as idproject FROM users WHERE UPPER(nom) LIKE UPPER(:query) OR UPPER(prenom) LIKE UPPER(:query) UNION SELECT NULL, idproject FROM project WHERE UPPER(title) LIKE UPPER(:query)")
    @RegisterBeanMapper(LookFor.class)
    List<LookFor> search(@Bind("query") String query);

}
