package fil.glp.wesearch.web.dao;

import fil.glp.wesearch.web.model.Laboratory;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface LaboratoryDao {

    @SqlQuery("SELECT * FROM laboratory WHERE idlab=:idlab")
    @RegisterBeanMapper(Laboratory.class)
    Laboratory findById(@Bind("idlab") int idlab);

    @SqlUpdate("insert into laboratory (idlab, libelle) values (:idlab, :libelle)")
    void insert(@BindBean() Laboratory laboratory);

    @SqlQuery("SELECT * FROM laboratory")
    @RegisterBeanMapper(Laboratory.class)
    List<Laboratory> all();

    @SqlUpdate("delete from laboratory where idlab = :idlab")
    void delete(@Bind("idlab") int idlab);

    @SqlUpdate("UPDATE * from laboratory where idlab=:idlab")
    int update(@Bind("idlab") int idlab);
}
