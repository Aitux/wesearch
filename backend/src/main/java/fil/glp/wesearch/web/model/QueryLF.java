package fil.glp.wesearch.web.model;

public class QueryLF {

    private String query;

    public QueryLF() {
    }

    public QueryLF(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = '%' + query + '%';
    }
}
