package fil.glp.wesearch.web.model;

import fil.glp.wesearch.web.dto.FollowDTO;

public class Follow {

    private int iduserfollow;
    private int iduserfollowed;

    public Follow() {
        super();
    }

    public int getIduserfollow() {
        return iduserfollow;
    }

    public void setIduserfollow(int iduserfollow) {
        this.iduserfollow = iduserfollow;
    }

    public int getIduserfollowed() {
        return iduserfollowed;
    }

    public void setIduserfollowed(int iduserfollowed) {
        this.iduserfollowed = iduserfollowed;
    }

    public FollowDTO toDTO() {
        FollowDTO dto = new FollowDTO();
        dto.setIduserfollow(this.iduserfollow);
        dto.setIduserfollowed(this.iduserfollowed);
        return dto;

    }
}
