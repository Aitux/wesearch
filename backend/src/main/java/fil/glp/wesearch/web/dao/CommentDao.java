package fil.glp.wesearch.web.dao;

import fil.glp.wesearch.web.model.Comment;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface CommentDao {

    @SqlQuery("SELECT * FROM comment WHERE idcomment=:idcomment")
    @RegisterBeanMapper(Comment.class)
    Comment findById(@Bind("idcomment") int idcomment);

    @SqlUpdate("insert into comment (idUsers, content, datecom) values (:idUsers, :content, :datecom)")
    @GetGeneratedKeys
    int insert(@BindBean() Comment comment);

    @SqlQuery("SELECT * FROM comment")
    @RegisterBeanMapper(Comment.class)
    List<Comment> all();

    @SqlUpdate("delete from comment where idcomment = :idcomment")
    void delete(@Bind("idcomment") int idcomment);

    @SqlUpdate("UPDATE * from comment where idcomment=:idcomment")
    int update(@Bind("idcomment") int idcomment);
}
