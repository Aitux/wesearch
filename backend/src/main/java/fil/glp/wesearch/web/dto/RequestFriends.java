package fil.glp.wesearch.web.dto;

import fil.glp.wesearch.util.Util;
import fil.glp.wesearch.web.controller.ProjectController;
import fil.glp.wesearch.web.controller.UserController;
import fil.glp.wesearch.web.exception.ItemNotFoundException;
import fil.glp.wesearch.web.model.Project;

import java.sql.Date;

public class RequestFriends {

    private Integer idcommentable;
    private Integer idpost;
    private Integer idusers;
    private String content;
    private Date datepost;
    private Integer iduserfollow;
    private Integer iduserfollowed;

    public RequestFriends() {
    }

    public RequestFriends(Integer idcommentable, Integer idpost, Integer idusers, String content, Date datepost, Integer iduserfollow, Integer iduserfollowed) {
        this.idcommentable = idcommentable;
        this.idpost = idpost;
        this.idusers = idusers;
        this.content = content;
        this.datepost = datepost;
        this.iduserfollow = iduserfollow;
        this.iduserfollowed = iduserfollowed;
    }

    public Integer getIdcommentable() {
        return idcommentable;
    }

    public void setIdcommentable(Integer idcommentable) {
        this.idcommentable = idcommentable;
    }

    public Integer getIdpost() {
        return idpost;
    }

    public void setIdpost(Integer idpost) {
        this.idpost = idpost;
    }

    public Integer getIdusers() {
        return idusers;
    }

    public void setIdusers(Integer idusers) {
        this.idusers = idusers;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDatepost() {
        return datepost;
    }

    public void setDatepost(Date datepost) {
        this.datepost = datepost;
    }

    public Integer getIduserfollow() {
        return iduserfollow;
    }

    public void setIduserfollow(Integer iduserfollow) {
        this.iduserfollow = iduserfollow;
    }

    public Integer getIduserfollowed() {
        return iduserfollowed;
    }

    public void setIduserfollowed(Integer iduserfollowed) {
        this.iduserfollowed = iduserfollowed;
    }

    public PostDTO toDto() {
        UserController uc = new UserController();
        System.out.println(getIduserfollowed());
        UsersDTO user = uc.findById(getIduserfollowed());
        PostDTO dto = new PostDTO();
        dto.setContent(Util.parseContent(content));
        dto.setDate(getDatepost());
        dto.setUsers(user);
        return dto;
    }


    public int compareTo(RequestFriends o) {
        return getDatepost().compareTo(o.getDatepost());
    }
}
