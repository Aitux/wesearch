package fil.glp.wesearch.web.dao;

import fil.glp.wesearch.web.model.Takepart;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface TakePartDao {
    @SqlQuery("SELECT * FROM takepart WHERE idusers=:idusers")
    @RegisterBeanMapper(Takepart.class)
    List<Takepart> findById(@Bind("idusers") Integer idusers);

    @SqlUpdate("insert into takepart (idusers, idproject) values (:idusers, :idproject)")
    void insert(@Bind("idusers") Integer idusers, @Bind("idproject") Integer idproject);

    @SqlQuery("SELECT * FROM takepart")
    @RegisterBeanMapper(Takepart.class)
    List<Takepart> all();

    @SqlUpdate("delete from takepart where idusers=:idusers AND idproject=:idproject")
    void delete(@Bind("idusers") Integer idusers, @Bind("idproject") Integer idproject);

    @SqlQuery("SELECT * FROM takepart WHERE idusers=:idusers AND idproject=:idproject")
    @RegisterBeanMapper(Takepart.class)
    Takepart takePart(@Bind("idusers") Integer idusers, @Bind("idproject") Integer idproject);
}