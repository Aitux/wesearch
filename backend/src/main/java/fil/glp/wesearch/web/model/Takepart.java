package fil.glp.wesearch.web.model;

import fil.glp.wesearch.web.dto.TakePartDTO;

public class Takepart {
	private int idUsers;
	private int idproject;
	
	public Takepart() {
		super();
	}

	public Takepart(int idUsers, int idproject, int iddomain, int idlab) {
		super();
		this.idUsers = idUsers;
		this.idproject = idproject;

	}

	public int getIdUsers() {
		return idUsers;
	}

	public void setIdUsers(int idUsers) {
		this.idUsers = idUsers;
	}

	public int getIdproject() {
		return idproject;
	}

	public void setIdproject(int idproject) {
		this.idproject = idproject;
	}

    public TakePartDTO toDTO() {
		TakePartDTO dto = new TakePartDTO();
		dto.setIdproject(getIdproject());
		dto.setIdUsers(getIdUsers());
		return dto;
    }
}
