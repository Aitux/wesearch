package fil.glp.wesearch.web.dao;

import fil.glp.wesearch.web.model.Project;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlBatch;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.Date;
import java.util.List;

public interface ProjectDao {

    @SqlQuery("SELECT * FROM project")
    @RegisterBeanMapper(Project.class)
    List<Project> all();

    @SqlUpdate("INSERT INTO project(idUsers, title, description,  startingdate, iddomain, idlab) values(:idUsers, :title, :description, :startingdate, 1,1)")
    @GetGeneratedKeys
    @RegisterBeanMapper(Project.class)
    Project insert(@Bind("idUsers") int idUsers, @Bind("title") String title, @Bind("description") String description, @Bind("startingdate") Date startingdate);

    @SqlUpdate("DELETE * FROM project where idProject=:idProject")
    void delete(@Bind("idProject") int idProject);

    @SqlQuery("SELECT * from project where idProject=:idProject")
    @RegisterBeanMapper(Project.class)
    Project findById(@Bind("idProject") int idProject);

    @SqlUpdate("UPDATE project set title =:title, description=:description where idproject=:idproject")
    void update(@BindBean() Project project);

    @SqlUpdate("UPDATE project set iddomain =:iddomain WHERE idproject=:idproject")
    void updateProject(@Bind("iddomain") Integer iddomain, @Bind("idproject") Integer idproject);
}
