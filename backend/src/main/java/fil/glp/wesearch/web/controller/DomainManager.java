package fil.glp.wesearch.web.controller;

import fil.glp.wesearch.api.BDDFactory;
import fil.glp.wesearch.web.dao.DomainDao;
import fil.glp.wesearch.web.dao.ProjectDao;
import fil.glp.wesearch.web.dao.StudyDao;
import fil.glp.wesearch.web.model.Domain;
import fil.glp.wesearch.web.model.Study;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class DomainManager {

    StudyDao studyDao = BDDFactory.buildDao(StudyDao.class);
    DomainDao domainDao = BDDFactory.buildDao(DomainDao.class);
    ProjectDao projectDao = BDDFactory.buildDao(ProjectDao.class);

    @PostMapping(value = "/domain", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Domain insert(@RequestBody Study s) {
        studyDao.insert(s.getIdUsers(), s.getIddomain());
        Domain d = new Domain();
        d.setIddomain(s.getIddomain());
        d.setLibelle(idToLabel(s.getIddomain()));
        return d;

    }

    @GetMapping(value = "/domain/{idproject}/{iddomain}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void changeProjectDomain(@PathVariable("idproject") Integer idproject, @PathVariable("iddomain") Integer iddomain) {
        projectDao.updateProject(iddomain, idproject);
    }

    @GetMapping(value = "/domain", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Domain> listall() {
        return domainDao.all();
    }

    public List<Domain> domainStudies(List<Study> studies) {
        List<Domain> domains = new ArrayList<>();
        for (Study s : studies) {
            domains.add(domainDao.findById(s.getIddomain()));
        }

        return domains;
    }

    public List<Study> getStudies(int idusers) {
        return studyDao.findById(idusers);
    }

    public String idToLabel(int iddomain) {
        return domainDao.findById(iddomain).getLibelle();
    }
}
