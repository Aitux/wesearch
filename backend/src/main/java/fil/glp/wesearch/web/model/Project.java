package fil.glp.wesearch.web.model;

import fil.glp.wesearch.web.controller.DomainManager;
import fil.glp.wesearch.web.controller.UserController;
import fil.glp.wesearch.web.dto.ProjectDTO;
import fil.glp.wesearch.web.dto.UsersDTO;

import java.util.Date;

public class Project {
	private Integer idcommentable;
	private Integer idproject;
	private Integer idUsers;
	private Integer iddomain;
	private Integer idlab;
	private String title;
	private String description;
	private Date startingdate;
	
	
    public Project() {
    }


	public Project(Integer idcommentable, Integer idproject, Integer idUsers, Integer iddomain, Integer idlab, String title,
			String description, Date startingdate) {
		super();
		this.idcommentable = idcommentable;
		this.idproject = idproject;
		this.idUsers = idUsers;
		this.iddomain = iddomain;
		this.idlab = idlab;
		this.title = title;
		this.description = description;
		this.startingdate = startingdate;
	}


	public Integer getIdcommentable() {
		return idcommentable;
	}


	public void setIdcommentable(Integer idcommentable) {
		this.idcommentable = idcommentable;
	}


	public Integer getIdproject() {
		return idproject;
	}


	public void setIdproject(Integer idproject) {
		this.idproject = idproject;
	}


	public Integer getIdUsers() {
		return idUsers;
	}


	public void setIdUsers(Integer idUsers) {
		this.idUsers = idUsers;
	}


	public Integer getIddomain() {
		return iddomain;
	}


	public void setIddomain(Integer iddomain) {
		this.iddomain = iddomain;
	}


	public Integer getIdlab() {
		return idlab;
	}


	public void setIdlab(Integer idlab) {
		this.idlab = idlab;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Date getStartingdate() {
		return startingdate;
	}


	public void setStartingdate(Date startingdate) {
		this.startingdate = startingdate;
	}

	public ProjectDTO toDto() {
		ProjectDTO dto = new ProjectDTO();
		UserController uc = new UserController();
		dto.setDescription(this.description);
		dto.setIdcommentable(this.idcommentable);
		dto.setCreator(uc.findById(getIdUsers()));
		dto.setDomain(new DomainManager().idToLabel(getIddomain()));
		dto.setIdlab(this.idlab);
		dto.setIdproject(this.idproject);
		dto.setStartingdate(this.startingdate);
		dto.setTitle(this.title);
		return dto;
	}
}
