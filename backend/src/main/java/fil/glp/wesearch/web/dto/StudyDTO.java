package fil.glp.wesearch.web.dto;

public class StudyDTO {

    private int idUsers;
    private int iddomain;

    public int getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(int idUsers) {
        this.idUsers = idUsers;
    }

    public int getIddomain() {
        return iddomain;
    }

    public void setIddomain(int iddomain) {
        this.iddomain = iddomain;
    }
}
