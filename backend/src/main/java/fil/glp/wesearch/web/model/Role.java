package fil.glp.wesearch.web.model;

import java.util.Objects;

public class Role {
	private int idrole;
	private String libelle;
		
	public Role() {
		super();
	}

	public Role(int idrole, String libelle) {
		super();
		this.idrole = idrole;
		this.libelle = libelle;
	}

	public int getIdrole() {
		return idrole;
	}

	public void setIdrole(int idrole) {
		this.idrole = idrole;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Role role = (Role) o;
		return idrole == role.idrole;
	}

	@Override
	public int hashCode() {
		return Objects.hash(idrole);
	}


	public boolean hasEnoughRight(Users users) {
		return idrole >= users.getIdrole();
	}
}
