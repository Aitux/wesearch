package fil.glp.wesearch.web.model;

public class Job {

    private Integer idjob;
    private String label;

    public Job() {
    }

    public Job(Integer idjob, String label) {
        this.idjob = idjob;
        this.label = label;
    }

    public Integer getIdjob() {
        return idjob;
    }

    public void setIdjob(Integer idjob) {
        this.idjob = idjob;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
