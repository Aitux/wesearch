package fil.glp.wesearch.web.security;

public class BooleanResponse {
    private Boolean response;

    public BooleanResponse(){}

    public BooleanResponse(Boolean s) {
        this.response = s;
    }

    public Boolean getResponse() {
        return response;
    }

    public void setResponse(Boolean response) {
        this.response = response;
    }
}
