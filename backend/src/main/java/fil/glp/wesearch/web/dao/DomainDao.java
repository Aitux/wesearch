package fil.glp.wesearch.web.dao;

import fil.glp.wesearch.web.model.Domain;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface DomainDao {

    @SqlQuery("SELECT * FROM domain WHERE iddomain=:iddomain")
    @RegisterBeanMapper(Domain.class)
    Domain findById(@Bind("iddomain") int iddomain);

    @SqlUpdate("insert into domain (iddomain, libelle) values (:iddomain, :libelle)")
    void insert(@Bind("iddomain") Integer iddomain, @Bind("libelle") String libelle);

    @SqlQuery("SELECT * FROM domain")
    @RegisterBeanMapper(Domain.class)
    List<Domain> all();

    @SqlUpdate("delete from domain where iddomain = :iddomain")
    void delete(@Bind("iddomain") int iddomain);

    @SqlUpdate("UPDATE * from domain where iddomain=:iddomain")
    int update(@Bind("iddomain") int iddomain);
}
