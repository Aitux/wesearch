package fil.glp.wesearch.web.controller;

import fil.glp.wesearch.api.BDDFactory;
import fil.glp.wesearch.web.dao.UsersDao;
import fil.glp.wesearch.web.model.Users;
import fil.glp.wesearch.web.security.BooleanResponse;
import fil.glp.wesearch.web.security.PageRequested;
import fil.glp.wesearch.web.security.RandomString;
import fil.glp.wesearch.web.security.StringResponse;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@RestController
public class LoginController {

    private UsersDao dao = BDDFactory.buildDao(UsersDao.class);

    private Map<String, Users> activeTokens = new HashMap<>();




    @PostMapping(value = "/authorize", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public StringResponse authorize(@RequestBody Users anon) {
        Users u = dao.authorize(anon.getMail(), anon.getPassword());
        if (Objects.isNull(u))
            return new StringResponse("invalid");
        else {
            RandomString rs = new RandomString();
            String str = rs.nextString();
            activeTokens.put(str, u);
            return new StringResponse(str, u.getIdusers());
        }
    }

    @PostMapping(value = "/validate", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BooleanResponse validate(@RequestBody PageRequested requested) {
        if (activeTokens.containsKey(requested.getToken())) {
            return new BooleanResponse(hasEnoughRight(activeTokens.get(requested.getToken()), requested.getAllowed()));
        }
        return new BooleanResponse(false);
    }

    @GetMapping(value = "/logout/{token}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BooleanResponse logout(@PathVariable("token") String token) {
        activeTokens.remove(token);
        return new BooleanResponse(true);
    }

    public boolean hasEnoughRight(Users users, int idrole) {
        return idrole >= users.getIdrole();
    }

    @GetMapping(value = "/connectedUser", produces = MediaType.APPLICATION_JSON_VALUE)
    public StringResponse connectedUser(){
        return new StringResponse(activeTokens.size()+"");
    }
}
