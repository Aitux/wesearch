package fil.glp.wesearch.web.model;

public class LookFor {

    private Integer idusers;
    private Integer idproject;

    public LookFor() {
    }

    public LookFor(Integer idusers, Integer idproject) {
        this.idusers = idusers;
        this.idproject = idproject;
    }

    public Integer getIdusers() {
        return idusers;
    }

    public void setIdusers(Integer idusers) {
        this.idusers = idusers;
    }

    public Integer getIdproject() {
        return idproject;
    }

    public void setIdproject(Integer idproject) {
        this.idproject = idproject;
    }
}
