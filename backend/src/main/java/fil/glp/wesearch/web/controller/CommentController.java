package fil.glp.wesearch.web.controller;


import fil.glp.wesearch.api.BDDFactory;
import fil.glp.wesearch.web.dao.CommentDao;
import fil.glp.wesearch.web.dao.IsaboutDao;
import fil.glp.wesearch.web.dto.CommentDTO;
import fil.glp.wesearch.web.model.Comment;
import fil.glp.wesearch.web.model.Isabout;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CommentController {

    IsaboutDao isaboutDao = BDDFactory.buildDao(IsaboutDao.class);
    CommentDao commentDao = BDDFactory.buildDao(CommentDao.class);

    @PostMapping(value = "/comment", consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    CommentDTO addComment(@RequestBody CommentDTO comment){
        Comment c = new Comment();
        c.setContent(comment.getContent());
        c.setDatecom(comment.getDate());
        c.setIdUsers(comment.getIdUsers());
        int id = commentDao.insert(c);

        Isabout is = new Isabout();
        is.setIdcomment(id);
        is.setIdCommentable(comment.getIdcommentable());
        isaboutDao.insert(is);
        return comment;
    }

    @GetMapping(value = "/comment/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    List<CommentDTO> getCommentForCommentable(@PathVariable("id") Integer id){
        List<Isabout> isList = isaboutDao.findById(id);
        List<CommentDTO> res = new ArrayList<>();
        for(Isabout i : isList){
            res.add(commentDao.findById(i.getIdcomment()).toDto());
        }
        return res;
    }
}
