package fil.glp.wesearch.web.model;

public class Laboratory {
	private int idlab;
	private String libelle;

	public Laboratory(){
	}
	
	public Laboratory(int idlab, String libelle){
		this.idlab=idlab;
		this.libelle=libelle;
	}

	public int getIdlab() {
		return idlab;
	}

	public void setIdlab(int idlab) {
		this.idlab = idlab;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

}
