package fil.glp.wesearch.web.model;

import com.google.common.base.Charsets;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import fil.glp.wesearch.web.controller.DomainManager;
import fil.glp.wesearch.web.controller.FollowController;
import fil.glp.wesearch.web.controller.JobManager;
import fil.glp.wesearch.web.controller.LaboratoryManager;
import fil.glp.wesearch.web.dto.UsersDTO;

import java.util.List;

public class Users {

    private Integer idusers;
    private Integer idlab;
    private String nom;
    private String prenom;
    private String mail;
    private String password;
    private String numero;
    private Integer idrole;
    private Integer idjob;


    public Users() {
        super();
    }

    public Integer getIdjob() {
        return idjob;
    }

    public void setIdjob(Integer idjob) {
        this.idjob = idjob;
    }

    public Integer getIdusers() {
        return idusers;
    }

    public void setIdusers(Integer idusers) {
        this.idusers = idusers;
    }

    public Integer getIdlab() {
        return idlab;
    }

    public void setIdlab(Integer idlab) {
        this.idlab = idlab;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getIdrole() {
        return idrole;
    }

    public void setIdrole(Integer idrole) {
        this.idrole = idrole;
    }

    public UsersDTO toDto() {
        FollowController fc = new FollowController();
        JobManager jm = new JobManager();
        LaboratoryManager lm = new LaboratoryManager();
        UsersDTO dto = new UsersDTO();
        try {
            dto.setLabo(lm.findById(getIdlab()).getLibelle());
        }catch (NullPointerException e){
            dto.setLabo("CRIStAL");
        }
        dto.setIdrole(this.idrole);
        dto.setIdUsers(this.idusers);
        dto.setMail(this.mail);
        dto.setNom(this.nom);
        dto.setNumero(this.numero);
        dto.setPrenom(this.prenom);
        dto.setNbAbo(fc.countFollow(this.getIdusers()).getNbfollow());
        dto.setJob(jm.findById(this.getIdjob()).getLabel());
        DomainManager dm = new DomainManager();
        dto.setDomains(dm.domainStudies(dm.getStudies(this.getIdusers())));
        return dto;
    }

    public String buildHash(String password) {
        Hasher hasher = Hashing.sha256().newHasher();
        hasher.putString(password, Charsets.UTF_8);
        return hasher.hash().toString();
    }

}
