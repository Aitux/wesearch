package fil.glp.wesearch.web.dto;

public class FollowDTO {

    private int iduserfollow;
    private int iduserfollowed;

    public int getIduserfollow() {
        return iduserfollow;
    }

    public void setIduserfollow(int iduserfollow) {
        this.iduserfollow = iduserfollow;
    }

    public int getIduserfollowed() {
        return iduserfollowed;
    }

    public void setIduserfollowed(int iduserfollowed) {
        this.iduserfollowed = iduserfollowed;
    }
}
