package fil.glp.wesearch.web.model;

import fil.glp.wesearch.web.dto.ProposeDTO;

public class Propose {
	private Integer idproject;
	private Integer idpropose;
	private String label;
	
	
	
	public Propose() {
		super();
	}

	public Integer getIdproject() {
		return idproject;
	}

	public void setIdproject(Integer idproject) {
		this.idproject = idproject;
	}

	public Integer getIdpropose() {
		return idpropose;
	}

	public void setIdpropose(Integer idpropose) {
		this.idpropose = idpropose;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public ProposeDTO toDto(){
		ProposeDTO res = new ProposeDTO();
		res.setIdproject(getIdproject());
		res.setId(getIdpropose());
		res.setLabel(getLabel());
		return res;
	}
}
