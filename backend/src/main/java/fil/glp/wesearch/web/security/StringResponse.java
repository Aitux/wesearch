package fil.glp.wesearch.web.security;

public class StringResponse {

    private String response;
    private Integer idUser;

    public StringResponse() {
    }

    public StringResponse(String s, Integer idUser) {
        this.response = s;
        this.idUser = idUser;
    }

    public StringResponse(String s) {
        this.response = s;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }
}