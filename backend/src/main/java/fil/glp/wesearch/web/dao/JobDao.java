package fil.glp.wesearch.web.dao;

import fil.glp.wesearch.web.model.Job;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

public interface JobDao {
    @SqlQuery("SELECT * FROM job WHERE idjob=:idjob")
    @RegisterBeanMapper(Job.class)
    Job findById(@Bind("idjob") Integer idjob);
}
