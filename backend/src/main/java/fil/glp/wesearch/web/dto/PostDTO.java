package fil.glp.wesearch.web.dto;

import java.util.Date;

public class PostDTO {

    private Integer idcommentable;
    private Integer idPost;
    private String content;
    private Date date;
    // User who wrote this comment
    private UsersDTO users;

    public Integer getIdcommentable() {
        return idcommentable;
    }

    public void setIdcommentable(Integer idcommentable) {
        this.idcommentable = idcommentable;
    }

    public Integer getIdPost() {
        return idPost;
    }

    public void setIdPost(Integer idPost) {
        this.idPost = idPost;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public UsersDTO getUsers() {
        return users;
    }

    public void setUsers(UsersDTO users) {
        this.users = users;
    }
}
