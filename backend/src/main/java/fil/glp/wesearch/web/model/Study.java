package fil.glp.wesearch.web.model;

public class Study {
	private int idUsers;
	private int iddomain;
	
	
	public Study() {
		super();
	}

	public Study(int idUsers, int iddomain) {
		super();
		this.idUsers = idUsers;
		this.iddomain = iddomain;
	}

	public int getIdUsers() {
		return idUsers;
	}

	public void setIdUsers(int idUsers) {
		this.idUsers = idUsers;
	}

	public int getIddomain() {
		return iddomain;
	}

	public void setIddomain(int iddomain) {
		this.iddomain = iddomain;
	}
	
	
	

}
