package fil.glp.wesearch.web.dao;

import fil.glp.wesearch.web.model.Role;
import fil.glp.wesearch.web.model.Users;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface RoleDao {

    @SqlQuery("SELECT * FROM role WHERE idrole=:idrole")
    @RegisterBeanMapper(Role.class)
    Role findById(@Bind("idrole") int idrole);

    @SqlUpdate("insert into role (idrole, libelle) values (:idrole, :libelle)")
    void insert(@BindBean() Role role);

    @SqlQuery("SELECT * FROM role")
    @RegisterBeanMapper(Role.class)
    List<Role> all();

    @SqlUpdate("delete from role where idrole = :idrole")
    void delete(@Bind("idrole") int idrole);

    @SqlUpdate("UPDATE * from role where idrole=:idrole")
    int update(@Bind("idrole") int idrole);
}
