package fil.glp.wesearch.web.dao;

import fil.glp.wesearch.web.model.Mentionned;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface MentionnedDao {

    @SqlQuery("SELECT * FROM mentionned WHERE idmentionned=:idmentionned")
    @RegisterBeanMapper(Mentionned.class)
    Mentionned findById(@Bind("idmentionned") int idmentionned);

    @SqlUpdate("insert into mentionned (idmentionned, idusers, idpost) values (:idmentionned, :idusers, :idpost)")
    void insert(@BindBean() Mentionned mentionned);

    @SqlQuery("SELECT * FROM mentionned")
    @RegisterBeanMapper(Mentionned.class)
    List<Mentionned> all();

    @SqlUpdate("delete from mentionned where idmentionned = :idmentionned")
    void delete(@Bind("idmentionned") int idmentionned);

    @SqlUpdate("UPDATE * from mentionned where idmentionned=:idmentionned")
    int update(@Bind("idmentionned") int idmentionned);
}
