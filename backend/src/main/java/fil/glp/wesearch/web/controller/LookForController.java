package fil.glp.wesearch.web.controller;

import fil.glp.wesearch.api.BDDFactory;
import fil.glp.wesearch.web.dao.LookForDao;
import fil.glp.wesearch.web.model.LookFor;
import fil.glp.wesearch.web.model.QueryLF;
import fil.glp.wesearch.web.model.ResponseQueryLF;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LookForController {

    LookForDao dao = BDDFactory.buildDao(LookForDao.class);

    @PostMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseQueryLF search(@RequestBody QueryLF qlf) {
        List<LookFor> lf = dao.search(qlf.getQuery());
        return ResponseQueryLF.parseList(lf);
    }


}