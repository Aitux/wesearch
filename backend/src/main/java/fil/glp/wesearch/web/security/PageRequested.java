package fil.glp.wesearch.web.security;

import fil.glp.wesearch.web.model.Role;

import java.util.List;

public class PageRequested {
    private String token;
    private int allowed;

    public PageRequested() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getAllowed() {
        return allowed;
    }

    public void setAllowed(int allowed) {
        this.allowed = allowed;
    }
}
