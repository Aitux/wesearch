package fil.glp.wesearch.web.dao;

import fil.glp.wesearch.web.dto.RequestFriends;
import fil.glp.wesearch.web.model.Post;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface PostDao {

    @SqlQuery("SELECT * FROM post WHERE idpost=:idpost")
    @RegisterBeanMapper(Post.class)
    Post findById(@Bind("idpost") int idpost);

    @SqlUpdate("INSERT INTO post (idusers, content, datepost) VALUES (:idusers, :content, :datepost)")
    void insert(@BindBean() Post post);

    @SqlQuery("SELECT * FROM post")
    @RegisterBeanMapper(Post.class)
    List<Post> all();

    @SqlUpdate("delete from post where idpost = :idpost")
    void delete(@Bind("idpost") int idpost);

    @SqlUpdate("UPDATE * from post where idpost=:idpost")
    int update(@Bind("idpost") int idpost);

    @SqlQuery("SELECT DISTINCT * FROM post WHERE idusers IN (SELECT iduserfollowed FROM follow WHERE iduserfollow=:idusers) OR idusers=:idusers")
    @RegisterBeanMapper(Post.class)
    List<Post> getFriendsPost(@Bind("idusers") int idusers);
}
