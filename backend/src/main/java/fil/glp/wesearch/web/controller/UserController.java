package fil.glp.wesearch.web.controller;

import fil.glp.wesearch.api.BDDFactory;
import fil.glp.wesearch.web.dao.*;
import fil.glp.wesearch.web.dto.PostDTO;
import fil.glp.wesearch.web.dto.ProjectDTO;
import fil.glp.wesearch.web.dto.RequestFriends;
import fil.glp.wesearch.web.dto.UsersDTO;
import fil.glp.wesearch.web.exception.ItemNotFoundException;
import fil.glp.wesearch.web.model.*;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * The type User controller.
 */
@RestController
public class UserController {

    /**
     * The Dao.
     */
    UsersDao dao = BDDFactory.buildDao(UsersDao.class);
    /**
     * The Project dao.
     */
    ProjectDao projectDao = BDDFactory.buildDao(ProjectDao.class);
    /**
     * The Post dao.
     */
    PostDao postDao = BDDFactory.buildDao(PostDao.class);
    /**
     * The Takepart dao.
     */
    TakePartDao takepartDao = BDDFactory.buildDao(TakePartDao.class);
    /**
     * The Follow dao.
     */
    FollowDao followDao = BDDFactory.buildDao(FollowDao.class);

    /**
     * Instantiates a new User controller.
     */
    public UserController() {
    }

    /**
     * All list.
     *
     * @return the list
     * @throws SQLException the sql exception
     */
    @GetMapping(value = "/user", produces = "application/json")
    @ResponseBody
    List<Users> all() throws SQLException {
        return dao.all();
    }

    /**
     * Find by id users dto.
     *
     * @param id the id
     * @return the users dto
     * @throws ItemNotFoundException the item not found exception
     */
    @GetMapping(value = "/user/{id}", produces = "application/json")
    public @ResponseBody
    UsersDTO findById(@PathVariable("id") int id) throws ItemNotFoundException {
        Users u = dao.findById(id);
        if(Objects.isNull(u))
            throw new ItemNotFoundException();
        return u.toDto();
    }

    /**
     * Insert users dto.
     *
     * @param newUser the new user
     * @return the users dto
     */
    @PostMapping(value = "/user")
    public UsersDTO insert(@RequestBody Users newUser) {
        return addUser(newUser);
    }

    /**
     * Gets feed.
     *
     * @param id the id
     * @return the feed
     */
    @GetMapping(value = "/user/{id}/feed", produces = "application/json")
    public @ResponseBody
    List<PostDTO> getFeed(@PathVariable("id") int id) {
        List<Post> list = postDao.getFriendsPost(id);
        List<PostDTO> dtos = new ArrayList<>();
        for (Post p : list) {
            dtos.add(p.toDto());
        }
        return dtos;
    }

    /**
     * Gets projects.
     *
     * @param id the id
     * @return the projects
     */
    @GetMapping(value = "/user/{id}/projects", produces = "application/json")
    public @ResponseBody
    List<ProjectDTO> getProjects(@PathVariable("id") int id) {
        List<Project> listpro = new ArrayList<>();
        List<Takepart> list = takepartDao.findById(id);
        for (Takepart p : list) {
            listpro.add(projectDao.findById(p.getIdproject()));
        }
        return listpro.stream().map(Project::toDto).collect(Collectors.toList());
    }

    /**
     * Gets friends.
     *
     * @param id the id
     * @return the friends
     */
    @GetMapping(value = "/user/{id}/friends", produces = "application/json")
    public @ResponseBody
    List<UsersDTO> getFriends(@PathVariable("id") int id) {
        List<Users> listpro = new ArrayList<>();
        List<Follow> list = followDao.findFollower(id);
        for (Follow p : list) {
            listpro.add(dao.findById(p.getIduserfollowed()));
        }
        return listpro.stream().map(Users::toDto).collect(Collectors.toList());
    }


    /**
     * Add user users dto.
     *
     * @param user the user
     * @return the users dto
     */
    UsersDTO addUser(Users user) {
        dao.insert(user);
        return user.toDto();
    }

    /**
     * Delete.
     *
     * @param id the id
     * @throws ItemNotFoundException the item not found exception
     */
    @DeleteMapping(value = "/user/{id}")
    public void delete(@PathVariable("id") int id) throws ItemNotFoundException {
        try {
            deleteUser(id);
        } catch (ItemNotFoundException e) {
            throw e;
        }
    }

    /**
     * Delete user.
     *
     * @param id the id
     */
    void deleteUser(int id) {
        dao.delete(id);
    }

    /**
     * Update the User
     * @param theUser
     * @throws ItemNotFoundException
     */
   @PutMapping(value = "/user")
    public void updateUser(@RequestBody Users theUser) throws ItemNotFoundException {
        dao.update(verifparam(theUser));
    }

    /**
     * Check if the new User's parameters aren't empty, and force to null if they are.
     * @param theUser
     * @return the User to update
     */
    public Users verifparam(Users theUser) {
        Users newUser = theUser;
        if(Objects.isNull(theUser.getIdlab())){
            newUser.setIdlab(null);
        }
        if(Objects.isNull(theUser.getMail())){
            newUser.setMail(null);
        }
        if(Objects.isNull(theUser.getNom())){
            newUser.setNom(null);
        }
        if(Objects.isNull(theUser.getPrenom())){
            newUser.setPrenom(null);
        }
         if(Objects.isNull(theUser.getNumero())){
            newUser.setNumero(null);
        }
         return newUser;
    }
}
