package fil.glp.wesearch.web.dao;

import fil.glp.wesearch.web.model.Search;
import fil.glp.wesearch.web.model.Study;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface StudyDao{

    @SqlQuery("SELECT * FROM study WHERE idusers=:idusers")
    @RegisterBeanMapper(Study.class)
    List<Study> findById(@Bind("idusers") int idusers);

    @SqlUpdate("insert into study (idusers, iddomain) values (:idusers, :iddomain)")
    void insert(@Bind("idusers") Integer idusers, @Bind("iddomain") Integer iddomain);

    @SqlQuery("SELECT * FROM study")
    @RegisterBeanMapper(Study.class)
    List<Study> all();

    @SqlUpdate("delete from study where idusers=:idusers AND iddomain=:iddomain")
    void delete(@Bind("idusers") int idusers, @Bind("iddomain") int iddomain);

    @SqlUpdate("UPDATE * from study where idusers=:idusers AND iddomain=:iddomain")
    int update(@Bind("idusers") int idusers, @Bind("iddomain") int iddomain);
}
