package fil.glp.wesearch.util;

import fil.glp.wesearch.web.controller.ProjectController;
import fil.glp.wesearch.web.controller.UserController;
import fil.glp.wesearch.web.dto.ProjectDTO;
import fil.glp.wesearch.web.dto.UsersDTO;
import fil.glp.wesearch.web.exception.ItemNotFoundException;

public class Util {

    public static String parseContent(String content) {
        StringBuilder stringBuilder = new StringBuilder();
        Integer j;
        StringBuilder str;
        Integer k;
        for (Integer i = 0; i < content.length(); i++) {
            k = i + 1;
            str = new StringBuilder();
            j = 0;
            switch (content.charAt(i)) {
                case '@':
                    while (k < content.length() && content.charAt(k) != ' ') {
                        j++;
                        str.append(content.charAt(k++));
                    }
                    stringBuilder.append(handleAt(str.toString()));
                    i += j;
                    break;
                case '!':
                    while (k < content.length() && content.charAt(k) != ' ') {
                        j++;
                        str.append(content.charAt(k++));
                    }
                    i += j;
                    stringBuilder.append(handleExclam(str.toString()));
                    break;
                case '#':
                    while (k < content.length() && content.charAt(k) != ' ') {
                        j++;
                        str.append(content.charAt(k++));
                    }
                    stringBuilder.append(handleHashtag());
                    i += j;
                    break;
                default:
                    stringBuilder.append(content.charAt(i));
            }
        }

        return stringBuilder.toString();
    }

    private static String handleHashtag() {
        return null;
    }

    private static String handleExclam(String str) {
        try {
            Integer id = Integer.parseInt(str);
            ProjectController pc = new ProjectController();
            ProjectDTO dto;
            try {
                dto = pc.findById(id);
            } catch (ItemNotFoundException e) {
                e.printStackTrace();
                return '!' + str;
            }

            return "<a class=\"project_ref\" href=\"/project?q=" + id + "\">!" + dto.getTitle() + "</a>";
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return '!' + str;
        }
    }

    private static String handleAt(String str) {
        try {
            Integer id = Integer.parseInt(str);
            UserController us = new UserController();
            UsersDTO dto;
            try {
                dto = us.findById(id);
            } catch (ItemNotFoundException e) {
                e.printStackTrace();
                return '@' + str;
            }

            return "<a class=\"user_ref\" href=\"/profile?q=" + id + "\">@" + dto.getPrenom() + " " + dto.getNom() + "</a>";
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return '@' + str;
        }
    }
}
