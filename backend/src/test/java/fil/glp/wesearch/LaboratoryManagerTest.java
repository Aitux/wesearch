package fil.glp.wesearch;

import fil.glp.wesearch.web.controller.LaboratoryManager;
import fil.glp.wesearch.web.model.Laboratory;
import org.junit.Test;

import static org.junit.Assert.fail;

public class LaboratoryManagerTest {


    public void contextLoads() {
    }

    @Test
    public void findById() {
        LaboratoryManager laboratoryManagerTest = new LaboratoryManager();
        Laboratory laboratoryTest = new Laboratory(1, "testlibelle");
        Laboratory laboratoryOutput = laboratoryManagerTest.findById(1);
        if (!(laboratoryOutput.getIdlab() == laboratoryTest.getIdlab())) {
            fail();
        }

    }

}
