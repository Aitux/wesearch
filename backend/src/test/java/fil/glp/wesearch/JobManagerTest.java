package fil.glp.wesearch;

import fil.glp.wesearch.web.controller.JobManager;
import fil.glp.wesearch.web.model.Job;
import org.junit.Test;

import static org.junit.Assert.fail;

public class JobManagerTest {

    public void contextLoads() {
    }

    @Test
    public void findByIdTest() {
        JobManager jobManagerTest = new JobManager();
        Job job = new Job(1, "testlibelle");
        Job joboutput = jobManagerTest.findById(1);
        if (!joboutput.getIdjob().equals(job.getIdjob())) {
            fail();
        }
    }
}
