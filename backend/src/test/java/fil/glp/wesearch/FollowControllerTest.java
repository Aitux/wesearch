package fil.glp.wesearch;

import fil.glp.wesearch.web.controller.FollowController;
import fil.glp.wesearch.web.dto.FollowDTO;
import fil.glp.wesearch.web.model.Follow;
import org.junit.Test;

import static org.junit.Assert.fail;

public class FollowControllerTest {


    public void contextLoads() {
    }


    @Test
    public void insert() {
        Follow followTest = new Follow();
        followTest.setIduserfollow(1);
        FollowController followControllerTest = new FollowController();
        FollowDTO followDTOTest = followControllerTest.insert(followTest);
        if (!(followDTOTest.getIduserfollow() == 1)) {
            fail();
        }
    }

}
