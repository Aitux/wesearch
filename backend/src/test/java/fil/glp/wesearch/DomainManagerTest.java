package fil.glp.wesearch;

import fil.glp.wesearch.api.BDDFactory;
import fil.glp.wesearch.web.controller.DomainManager;
import fil.glp.wesearch.web.dao.DomainDao;
import fil.glp.wesearch.web.dao.StudyDao;
import fil.glp.wesearch.web.model.Domain;
import fil.glp.wesearch.web.model.Study;
import fil.glp.wesearch.web.model.Users;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DomainManagerTest {

    StudyDao studyDaoTest = BDDFactory.buildDao(StudyDao.class);
    DomainDao domainDaoTest = BDDFactory.buildDao(DomainDao.class);

    public void contextLoads() {
    }

    @Test
    public void domainStudiesTest() {
        Study studyTest1 = new Study();
        studyTest1.setIddomain(1);
        Study studyTest2 = new Study();
        studyTest2.setIddomain(2);

        List<Study> ListStudyTest = new ArrayList<>();
        ListStudyTest.add(studyTest1);
        ListStudyTest.add(studyTest2);

        DomainManager domainManagerTest = new DomainManager();

        List<Domain> ListStudyOutputTest = new ArrayList<>();
        ListStudyOutputTest = domainManagerTest.domainStudies(ListStudyTest);

        List<Domain> ListDomainInputTest = new ArrayList<>();
        Domain domain1 = new Domain();
        domain1.setIddomain(1);
        Domain domain2 = new Domain();
        domain2.setIddomain(2);
        ListDomainInputTest.add(domain1);
        ListDomainInputTest.add(domain2);

        assertEquals(ListDomainInputTest, ListStudyOutputTest);

    }

    @Test
    public void getStudiesTest() {
        Users usersTest = new Users();
        Domain domain = new Domain();
        Study study = new Study(usersTest.getIdusers(), domain.getIddomain());
        List<Study> listOutput = new ArrayList<>();
        listOutput.add(study);

        int idUsersTest = usersTest.getIdusers();
        List<Study> ListStudyOutput = new ArrayList<>();
       // ListStudyOutput = domainManagerTest.getStudies(idUsersTest);

        assertEquals(listOutput, ListStudyOutput);
    }


    @Test
    public void idToLabeltest() {
        Domain domain = new Domain(101, "testdomaine");
     //   String testlibelle = domainManagerTest.idToLabel(101);
     //   assertEquals("testdomaine", testlibelle);
    }

}
