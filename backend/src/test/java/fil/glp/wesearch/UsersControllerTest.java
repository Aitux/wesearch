package fil.glp.wesearch;

import fil.glp.wesearch.web.controller.UserController;
import fil.glp.wesearch.web.dto.UsersDTO;
import fil.glp.wesearch.web.exception.ItemNotFoundException;
import fil.glp.wesearch.web.model.Users;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class UsersControllerTest {

	public void contextLoads() {
	}


	@Test
	public void VerifParamWithNoProblem() {
		UserController userController = new UserController();
		Users users = new Users();
		users.setIdlab(999);
		users.setMail("test@testcontroller.test");
		users.setNom("test");
		users.setPrenom("test");
		users.setNumero(String.valueOf(0000000000));

		//Il faut mettre verifparam en public..
		Users userTest = userController.verifparam(users);

		assertEquals(users.getIdlab(), userTest.getIdlab());
		assertEquals(users.getMail(), userTest.getMail());
		assertEquals(users.getNom(), userTest.getNom());
		assertEquals(users.getPrenom(), userTest.getPrenom());
		assertEquals(users.getNumero(), userTest.getNumero());
	}

	@Test
	public void VerifParamWithProblem() {
		UserController userController = new UserController();
		Users users = new Users();

		//Il faut mettre verifparam en public..
		Users userTest = userController.verifparam(users);

		assertEquals(null, userTest.getIdlab());
		assertEquals(null, userTest.getMail());
		assertEquals(null, userTest.getNom());
		assertEquals(null, userTest.getPrenom());
		assertEquals(null, userTest.getNumero());
	}

	@Test
	public void findById(){
		UserController userController = new UserController();
		try {
			userController.findById(-1);
			fail("Erreur pas d'exception levée");
		}
		catch (ItemNotFoundException e){
        System.out.println("Le test est passé, exception levée");
		}
	}

	@Test
	public void insertTest(){
		UserController userController = new UserController();
		Users users= new Users();
		UsersDTO usersDTO= userController.insert(users);
		if (usersDTO.getIdUsers()==null) {
			fail();
		}
	}


}
