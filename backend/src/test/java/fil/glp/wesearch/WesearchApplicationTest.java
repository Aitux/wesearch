package fil.glp.wesearch;

import com.google.common.base.Charsets;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import fil.glp.wesearch.web.model.Post;
import fil.glp.wesearch.web.model.Role;
import fil.glp.wesearch.web.model.Users;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import java.util.Date;

import static org.junit.Assert.*;

public class WesearchApplicationTest {

	public void contextLoads() {
	}

	@Test
	public void UsersBuildHash2() {
		assertEquals(0,0);
	}



	@Test
	public void UsersBuildHash() {
		Users users=new Users();
		String atester="hello";
		Hasher hasher = Hashing.sha256().newHasher();
		hasher.putString(atester, Charsets.UTF_8);
		assertEquals(hasher.hash().toString(), users.buildHash("hello"));
	}

	@Test
	public void RoleTrue() {
		Role role=new Role();
		Object o = new Role();
		assertTrue(role.equals(o));
	}

	@Test
	public void RoleFalse() {
		Role role=new Role();
		Object o=null;
		assertFalse(role.equals(o));
	}

	@Test
	public void PostParsecase5() {
		Date date= new Date();
		Post post = new Post(500,500,"@",date,500);
		int k=0;
		int j=0;
		StringBuilder str = null;
		while (k < post.getContent().length() && post.getContent().charAt(k) != ' ') {
			j++;
			str.append(post.getContent().charAt(k++));
		}
	}

	@Test
	public void PostParsecase2() {
		Date date=new Date();
		Post post = new Post(500,500,"!",date,500);
		int k=0;
		int j=0;
		StringBuilder str = null;
		while (k < post.getContent().length() && post.getContent().charAt(k) != ' ') {
			j++;
			str.append(post.getContent().charAt(k++));
		}
	}
	@Test
	public void PostParsecase1() {
		Date date=new Date();
		Post post = new Post(500,500,"@",date,500);
		int k=0;
		int j=0;
		StringBuilder str = null;
		while (k < post.getContent().length() && post.getContent().charAt(k) != ' ') {
			j++;
			str.append(post.getContent().charAt(k++));
		}
	}
	@Test
	public void PostParsecase3() {
		Date date=new Date();
		Post post = new Post(500,500,"#",date,500);
		int k=0;
		int j=0;
		StringBuilder str = null;
		while (k < post.getContent().length() && post.getContent().charAt(k) != ' ') {
			j++;
			str.append(post.getContent().charAt(k++));
		}
	}

}
