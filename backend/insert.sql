insert.sql edited online with Bitbucket--Laboratory insert

INSERT into Laboratory values (0,'ALITHILA');
INSERT into Laboratory values (1,'CEAC');
INSERT into Laboratory values (2,'CERAPS');
INSERT into Laboratory values (3,'CECILLE');
INSERT into Laboratory values (4,'CHJ');
INSERT into Laboratory values (5,'CRISTAL');
INSERT into Laboratory values (6,'JPArc');
INSERT into Laboratory values (8,'CRDP');
INSERT into Laboratory values (9,'CIIL');
INSERT into Laboratory values (10,'CIREL');
INSERT into Laboratory values (11,'CLERSE');
INSERT into Laboratory values (12,'EPS');
INSERT into Laboratory values (13,'EVO ECO PALEO');
INSERT into Laboratory values (14,'EPS');
INSERT into Laboratory values (15,'ETUDIANT');
INSERT into Laboratory values (16,'PERSONNEL');

--Domain insert

INSERT into Domain values (0,'ARTS');
INSERT into Domain values (1,'LANGUES');
INSERT into Domain values (2,'SCIENCES HUMAINES');
INSERT into Domain values (3,'DROIT');
INSERT into Domain values (4,'ECONOMIE');
INSERT into Domain values (5,'GESTION');
INSERT into Domain values (6,'UMR');
INSERT into Domain values (7,'SCIENCES ET TECH');
INSERT into Domain values (8,'SVT');
INSERT into Domain values (9,'SANTE');
INSERT into Domain values (10,'EA');
INSERT into Domain values (11,'USR');
INSERT into Domain values (12,'UMR-S');

--Role insert

INSERT into Role values (0,'Admin');
INSERT into Role values (1,'User');

--Commentable insert
--Abstrait table

INSERT into Commentable values (0);
INSERT into Commentable values (1);
INSERT into Commentable values (2);
INSERT into Commentable values (3);
INSERT into Commentable values (4);

--Tag Insert

INSERT into Tag values (0,'informatique','science');
INSERT into Tag values (1,'svt','science');
INSERT into Tag values (2,'mecanique','science');
INSERT into Tag values (3,'sport','sante');
INSERT into Tag values (4,'eps','sante');
INSERT into Tag values (5,'corpo','droit');
INSERT into Tag values (6,'langue','sciences humaines');
INSERT into Tag values (7,'economie','sciences humaines');
INSERT into Tag values (8,'dessin','arts');
INSERT into Tag values (9,'arts plastiques','arts');


--Job insert

INSERT INTO Job values (0,'Personnel');
INSERT INTO Job values (1,'Etudiant');
INSERT INTO Job values (2,'Doctorant');
INSERT INTO Job values (3,'Professeur');
INSERT INTO Job values (4,'Maitre de conférence');
INSERT INTO Job values (5,'Chercheur');

--Users Insert
--Ajout de l'inscription

INSERT into Users values (0,5,'Mathieu','Philippe','philippe.mathieu@prof.fr','pw1',0000000000,'2020-01-22',4,1);
INSERT into Users values (1,5,'Jean-Christophe','Routier','jean-c.routier@prof.fr','pw1',0000000000,'2020-01-22',4,1);
INSERT into Users values (2,5,'Bruno','Beaufils','bruno.beaufils@prof.fr','pw1',0000000000,'2020-01-22',4,1);
INSERT into Users values (3,5,'Anne-C','Caron','Anne-C.Caron@prof.fr','pw1',0000000000,'2020-01-22',4,1);
INSERT into Users values (4,5,'Patricia','Everaere','patricia.everaere@prof.fr','pw1',0000000000,'2020-01-22',4,1);
INSERT into Users values (5,5,'Antoine','Nongaillard','antoine.nongaillard@prof.fr','pw1',0000000000,'2020-01-22',3,1);
INSERT into Users values (6,5,'Yann','Secq','Yann.Secq@prof.fr','pw1',0000000000,'2020-01-22',3,1);
INSERT into Users values (7,5,'Corwin','Fevre','corwin.fevre@prof.fr','pw1',0000000000,1,'2020-01-22',2,1);
INSERT into Users values (8,5,'Ningkui','Wang','Ningkui.Wang@prof.fr','pw1',0000000000,1,'2020-01-22',2,1);
INSERT into Users values (9,15,'Hamadi','hamadi','hamadi@h.fr','pw1',0000000000,'2020-01-22',1,1);
INSERT into Users values (10,2,'ALAM','Thomas','alam.Thomas@prof.fr','pw1',0000000000,'2020-01-22',5,1);
INSERT into Users values (11,6,'Luc','Buee','luc.buee@prof.fr','pw1',0000000000,'2020-01-22',5,1);
INSERT into Users values (12,9,'Jérémie','Montfort','jeje.monmon@prof.fr','pw1',0000000000,'2020-01-22',5,1);
INSERT into Users values (13,9,'Laurent','Storme','laurent.stome@prof.fr','pw1',0000000000,'2020-01-22',3,1);
INSERT into Users values (14,11,'Philippe','Froguel','philippe.froguel@prof.fr','pw1',0000000000,'2020-01-22',3,1);
INSERT into Users values (15,11,'Herve','Leleur','herve.leleu@etu.fr','pw1',0000000000,'2020-01-22',1,1);
INSERT into Users values (16,15,'Alexandre','Devassine','a.devassine@orange.fr','alexandre',0682758646,'2020-01-22',1,1);
INSERT into Users values (17,15,'Yann','Fablet','yann.fablette59@gunmail.fr','yann',0607080910,'2020-01-22',1,1);
INSERT into Users values (18,15,'Simon','Vdp','aituxlechips@xXXXXXX.com','simon',0123456789,'2020-01-22',1,1);
INSERT into Users values (19,15,'Oceane','Pruvost','ocedu62@boulognerepresente.fr','océane',0123456789,'2020-01-22',1,1);
INSERT into Users values (20,15,'Aghiles','Terbah','aghi@panzani.x','glpjetaime',0645789563,'2020-01-22',1,1);
INSERT into Users values (21,15,'Admin','Admin','admin@admin.fr','admin',0102030405,'2020-01-22',0,0);


--Study insert
INSERT into Study values (0,7);
INSERT into Study values (1,7);
INSERT into Study values (2,7);
INSERT into Study values (3,7);
INSERT into Study values (4,7);
INSERT into Study values (5,7);
INSERT into Study values (6,7);
INSERT into Study values (7,7);
INSERT into Study values (8,7);
INSERT into Study values (9,7);
INSERT into Study values (10,11);
INSERT into Study values (11,12);
INSERT into Study values (12,11);
INSERT into Study values (13,9);
INSERT into Study values (14,9);
INSERT into Study values (15,3);
INSERT into Study values (16,7);
INSERT into Study values (17,7);
INSERT into Study values (18,7);
INSERT into Study values (19,7);
INSERT into Study values (20,5);

--Comment insert

INSERT into Comment values (0,1,'Il n y a que moi qui travaille ici ? ','2020-01-22');
INSERT into Comment values (1,17,'Tres bonne application et bien fluide comme j aime !','2020-01-23');
INSERT into Comment values (2,3,'Bonne année à tout le monde !','2020-01-01');
INSERT into Comment values (3,1,'Bonjour, ceci est mon premier commentaire =)','2020-01-22');
INSERT into Comment values (4,18,'Franchement cette appli est stylé !','2020-01-22');
INSERT into Comment values (5,18,'Bon les gars on finit le sprint demain  =)','2020-01-22');
INSERT into Comment values (6,16,'Pas de problème.','2020-01-22');
INSERT into Comment values (7,17,'Je ne suis pas un héro, je suis juste un data scientist','2020-01-22');
INSERT into Comment values (8,19,'Mais c est quoi toutes ces donnes','2020-01-20');
INSERT into Comment values (9,17,'je veux m inserer','2020-01-21');
INSERT into Comment values (10,16,'je m eclate a rentrer des donnees','2020-01-17');
INSERT into Comment values (11,16,'coucou, c est encore moi','2020-01-17');
INSERT into Comment values (12,21,'je suis administrateur et je prens la grosse tete','2020-01-18');
INSERT into Comment values (13,21,'Bonne annee aux utilisateurs','2020-01-01');
INSERT into Comment values (14,15,'je ne veux pas passer le toeic','2020-01-16');


--Post insert
INSERT into Post values (0,0,1,'On va devoir travailler ','2020-01-22');
INSERT into Post values (0,1,17,'On va manger des bonnes pates','2020-01-23');
INSERT into Post values (1,2,3,'Aujourd hui on fait un cours complexes','2020-01-01');
INSERT into Post values (1,3,1,'Je me suis mis au sport','2020-01-13');
INSERT into Post values (1,4,18,'L ecologie c est le futur','2020-01-22');
INSERT into Post values (2,5,18,'Je suis en train de migrer un projet','2020-01-18');
INSERT into Post values (2,6,16,'GOGOGO !','2020-01-22');
INSERT into Post values (2,7,17,'je vais changer de banques','2020-01-21');
INSERT into Post values (2,8,19,'Nausica meilleur parc au monde','2020-01-23');
INSERT into Post values (3,9,17,'je vais acheter une BMW mais j ai pas les sous...','2020-01-24');
INSERT into Post values (3,10,16,'GO TO Luxembourg','2020-01-18');
INSERT into Post values (3,11,16,'je raconte ma vie','2020-01-22');
INSERT into Post values (4,12,21,'A bah peut etre pas en faite','2020-01-14');
INSERT into Post values (4,13,21,'J aime la GLP','2020-01-01');
INSERT into Post values (4,14,15,'Il Faut que je change de banque','2020-01-27');

--Mentionned insert

INSERT into Mentionned values (0,1,1);
INSERT into Mentionned values (1,1,12);
INSERT into Mentionned values (2,3,4);
INSERT into Mentionned values (3,8,12);
INSERT into Mentionned values (4,8,13);
INSERT into Mentionned values (5,2,2);
INSERT into Mentionned values (6,3,2);
INSERT into Mentionned values (7,8,7);
INSERT into Mentionned values (8,16,1);
INSERT into Mentionned values (9,17,1);
INSERT into Mentionned values (10,18,1);
INSERT into Mentionned values (11,19,1);
INSERT into Mentionned values (12,20,1);
INSERT into Mentionned values (13,10,1);


--Project insert 

INSERT into Project values (0,0,7,5,'Informatique','refonte de la base de donnee de toute la FAC de Lille','2020-01-22');
INSERT into Project values (0,1,7,5,'Informatique','refonte de la base de donnee de toute la FAC de Lille','2020-01-22');
INSERT into Project values (0,2,7,5,'Informatique','refonte de la base de donnee de toute la FAC de Lille','2020-01-22');
INSERT into Project values (0,3,7,5,'Informatique','refonte de la base de donnee de toute la FAC de Lille','2020-01-22');
INSERT into Project values (0,4,7,5,'Informatique','refonte de la base de donnee de toute la FAC de Lille','2020-01-22');
INSERT into Project values (0,5,7,5,'Informatique','refonte de la base de donnee de toute la FAC de Lille','2020-01-22');
INSERT into Project values (1,16,7,15,'GLP','Application pour faire rencontrer tous les profs, enseignants et etudiants de la fac','2020-01-22');
INSERT into Project values (1,17,7,15,'GLP','Application pour faire rencontrer tous les profs, enseignants et etudiants de la fac','2020-01-22');
INSERT into Project values (1,18,7,15,'GLP','Application pour faire rencontrer tous les profs, enseignants et etudiants de la fac','2020-01-22');
INSERT into Project values (1,19,7,15,'GLP','Application pour faire rencontrer tous les profs, enseignants et etudiants de la fac','2020-01-22');
INSERT into Project values (2,16,7,15,'Informatique','refonte de la base de donnee de toute la FAC de Lille','2020-02-22');
INSERT into Project values (3,21,7,15,'Informatique','refonte de la base de donnee de toute la FAC de Lille','2020-01-22');
INSERT into Project values (4,1,7,5,'Informatique','recherche sur l automatisation des taches de developpement','2020-01-22');

--About insert
INSERT into Isabout values (0,0,1);
INSERT into Isabout values (0,1,17);
INSERT into Isabout values (1,2,3);
INSERT into Isabout values (1,3,1);
INSERT into Isabout values (2,4,18);
INSERT into Isabout values (3,5,18);
INSERT into Isabout values (3,6,16);
INSERT into Isabout values (3,7,17);
INSERT into Isabout values (4,8,19);


--Propose insert
INSERT into Propose values (0,0,0,0,0);
INSERT into Propose values (0,1,1,1,1);
INSERT into Propose values (1,2,5,1,2);
INSERT into Propose values (1,1,5,0,0);
INSERT into Propose values (0,3,0,0,5);
INSERT into Propose values (2,0,0,0,6);
INSERT into Propose values (1,0,0,0,6);
INSERT into Propose values (3,0,0,0,15);
INSERT into Propose values (4,1,2,3,2);
INSERT into Propose values (1,4,2,3,5);

--Search insert

INSERT into Search values (0,0,0,0,0);
INSERT into Search values (0,1,1,1,1);
INSERT into Search values (1,2,5,1,2);
INSERT into Search values (1,1,5,0,0);
INSERT into Search values (0,3,0,0,5);
INSERT into Search values (2,0,0,0,6);
INSERT into Search values (1,0,0,0,6);
INSERT into Search values (3,0,0,0,15);
INSERT into Search values (4,1,2,3,2);
INSERT into Search values (1,4,2,3,5);

--TakePart insert
INSERT into Takepart values (1,0,1,1);
INSERT into Takepart values (2,0,1,1);
INSERT into Takepart values (3,0,2,2);
INSERT into Takepart values (4,0,2,3);
INSERT into Takepart values (5,0,2,3);
INSERT into Takepart values (6,1,3,3);
INSERT into Takepart values (7,1,4,3);
INSERT into Takepart values (8,1,8,4);
INSERT into Takepart values (9,1,2,5);
INSERT into Takepart values (10,1,2,5);
INSERT into Takepart values (11,2,3,6);
INSERT into Takepart values (12,2,4,5);
INSERT into Takepart values (13,2,5,5);
INSERT into Takepart values (14,2,6,6);
INSERT into Takepart values (15,3,7,7);
INSERT into Takepart values (16,3,8,8);
INSERT into Takepart values (17,4,9,8);
INSERT into Takepart values (18,4,10,8);
INSERT into Takepart values (19,4,11,12);
INSERT into Takepart values (20,4,12,14);

--Follow insert
INSERT INTO Follow values(0,2);
INSERT INTO Follow values(0,3);
INSERT INTO Follow values(0,4);
INSERT INTO Follow values(0,5);
INSERT INTO Follow values(0,6);
INSERT INTO Follow values(1,0);
INSERT INTO Follow values(3,2);
INSERT INTO Follow values(4,2);
INSERT INTO Follow values(12,13);
INSERT INTO Follow values(13,12);
INSERT INTO Follow values(17,18);
INSERT INTO Follow values(16,19);
INSERT INTO Follow values(19,21);
INSERT INTO Follow values(20,16);
INSERT INTO Follow values(16,12);
INSERT INTO Follow values(13,12);
INSERT INTO Follow values(16,14);
INSERT INTO Follow values(15,16);
INSERT INTO Follow values(20,21);













