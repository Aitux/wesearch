CREATE TABLE Laboratoire(
idLabo integer,
libelle varchar,
description varchar);

CREATE TABLE Equipe(
idEquipe integer,
libelle varchar,
description varchar,
idLabo integer);

CREATE TABLE Utilisateur(
idUtilisateur integer,
nom varchar,
prenom varchar,
mail varchar,
mdp varchar,
numero varchar,
statut varchar,
idEquipe integer);

CREATE TABLE Projet(
idProjet integer,
libelle varchar,
description varchar,
duree timestamp,
idDomaine integer,
idUtilisateur integer);

CREATE TABLE Publication(
idPublication integer,
titre varchar,
datePubli datetime,
idProjet integer,
idUtilisateur integer);

CREATE TABLE Domaine(
idDomaine integer,
libelle varchar);

CREATE TABLE Abonnement(
idAbo integer,
idUtilisateur integer);

CREATE TABLE ProjDom(
idProjet integer,
idDomaine integer);
