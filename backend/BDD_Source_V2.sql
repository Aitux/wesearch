 /* DROP TABLE DES TABLES Permettant d'avoir une BDD propre */

DROP TABLE IF EXISTS Laboratory CASCADE;
DROP TABLE IF EXISTS Domain CASCADE;
DROP TABLE IF EXISTS Role CASCADE;
DROP TABLE IF EXISTS Commentable CASCADE;
DROP TABLE IF EXISTS Tag CASCADE;
DROP TABLE IF EXISTS Job CASCADE;
DROP TABLE IF EXISTS Users CASCADE;
DROP TABLE IF EXISTS Study CASCADE;
DROP TABLE IF EXISTS Comment CASCADE;
DROP TABLE IF EXISTS Post CASCADE;
DROP TABLE IF EXISTS Mentionned CASCADE;
DROP TABLE IF EXISTS Project CASCADE;
DROP TABLE IF EXISTS Isabout CASCADE;
DROP TABLE IF EXISTS Propose CASCADE;
DROP TABLE IF EXISTS Search CASCADE;
DROP TABLE IF EXISTS Takepart CASCADE;
DROP TABLE IF EXISTS Follow CASCADE;

CREATE TABLE Laboratory(
  idlab SERIAL,
  libelle varchar (50)
);
ALTER TABLE Laboratory ADD PRIMARY KEY(idlab);

CREATE TABLE Domain(
  iddomain SERIAL,
  libelle varchar(50)
);
ALTER TABLE Domain ADD PRIMARY KEY(iddomain);

CREATE TABLE Role(
  idrole SERIAL,
  libelle varchar(50)
);
ALTER TABLE Role ADD PRIMARY KEY(idrole);

--C est une table abstraite
CREATE TABLE Commentable(
  idcommentable SERIAL
);
ALTER TABLE Commentable ADD PRIMARY KEY(idcommentable);

CREATE TABLE Tag(
idtag SERIAL,
libelle varchar(50)
);
ALTER TABLE Tag ADD PRIMARY KEY(idtag);

CREATE TABLE Job(
idJob SERIAL,
label varchar(50)
);
ALTER TABLE Job ADD PRIMARY KEY (idJob);

CREATE TABLE Users(
  idUsers SERIAL,
  idlab INTEGER,
  nom varchar(50),
  prenom varchar(50),
  mail varchar(50),
  password varchar(50),
  numero varchar(50),
  inscription DATE,
  idJob INTEGER NOT NULL,
  idrole INTEGER  
  Pic Bytea;
);
ALTER TABLE Users ADD PRIMARY KEY(idUsers);
ALTER TABLE Users ADD CONSTRAINT FK_Users_lab FOREIGN KEY (idlab) REFERENCES Laboratory(idlab);
ALTER TABLE Users ADD CONSTRAINT FK_Users_role FOREIGN KEY (idrole) REFERENCES Role(idrole);
ALTER TABLE Users ADD CONSTRAINT FK_Users_idjob FOREIGN KEY (idJob) REFERENCES Job(idJob);


CREATE TABLE Study(
idUsers INTEGER NOT NULL,
iddomain INTEGER NOT NULL
);

ALTER TABLE Study ADD PRIMARY KEY(idUsers,iddomain);
ALTER TABLE Study ADD CONSTRAINT FK_Study_user FOREIGN KEY (idUsers) REFERENCES Users(idUsers);
ALTER TABLE Study ADD CONSTRAINT FK_Study_iddomain FOREIGN KEY (iddomain) REFERENCES Domain(iddomain);

CREATE TABLE Comment(
  idcomment SERIAL,
  idUsers INTEGER NOT NULL,
  content varchar(500),
  datecom DATE
);
ALTER TABLE Comment ADD PRIMARY KEY(idcomment);
ALTER TABLE Comment ADD CONSTRAINT FK_Comment_Users FOREIGN KEY (idUsers) REFERENCES Users(idUsers);

-- Table qui herite de Commentable
CREATE TABLE Post(
  idpost SERIAL,
  idUsers INTEGER NOT NULL,
  content varchar(550),
  datepost DATE
)INHERITS (Commentable);

ALTER TABLE Post ADD PRIMARY KEY(idpost);
ALTER TABLE Post ADD CONSTRAINT FK_Post_Users FOREIGN KEY (idUsers) REFERENCES Users(idUsers);

CREATE TABLE Mentionned (
  idMentionned SERIAL,
  idUsers INTEGER NOT NULL,
  idpost INTEGER NOT NULL
);

ALTER TABLE Mentionned ADD PRIMARY KEY(idMentionned);
ALTER TABLE Mentionned ADD CONSTRAINT Fk_Mentionned_idpost FOREIGN KEY (idpost) REFERENCES Post(idpost);
ALTER TABLE Mentionned ADD CONSTRAINT Fk_Mentionned_idusers FOREIGN KEY (idusers) REFERENCES Users(idusers);


--Table qui herite de Commentable
CREATE TABLE Project(
  idproject SERIAL,
  idUsers INTEGER NOT NULL,
  iddomain INTEGER NOT NULL,
  idlab INTEGER NOT NULL,
  title varchar(50),
  description varchar(550),
  startingdate DATE
)INHERITS (Commentable);

ALTER TABLE Project ADD PRIMARY KEY(idproject);
ALTER TABLE Project ADD CONSTRAINT FK_Project_idUsers FOREIGN KEY(idUsers) REFERENCES Users(idUsers);
ALTER TABLE Project ADD CONSTRAINT FK_Project_iddomain FOREIGN KEY(iddomain) REFERENCES Domain(iddomain);
ALTER TABLE Project ADD CONSTRAINT FK_Project_idlab FOREIGN KEY(idlab) REFERENCES Laboratory(idlab);

CREATE TABLE Isabout(
  idcommentable INTEGER NOT NULL,
  idcomment INTEGER NOT NULL,
);

ALTER TABLE Isabout ADD PRIMARY KEY(idcommentable,idcomment);
ALTER TABLE Isabout ADD CONSTRAINT FK_Isabout_idcommentable FOREIGN KEY (idcommentable) REFERENCES Commentable(idcommentable);
ALTER TABLE Isabout ADD CONSTRAINT FK_Isabout_idcomment FOREIGN KEY (idcomment) REFERENCES Comment(idcomment);


CREATE TABLE Propose (
  idpropose SERIAL,
  idproject INTEGER NOT NULL,
  label varchar(50)
);

ALTER TABLE Propose ADD PRIMARY KEY(idpropose);
ALTER TABLE Propose ADD CONSTRAINT FK_Propose_idproject FOREIGN KEY (idproject) REFERENCES Project(idproject);


CREATE TABLE Search (
  idsearch SERIAL,
  idproject INTEGER NOT NULL,
  label varchar(50)
);
ALTER TABLE Search ADD PRIMARY KEY(idsearch);
ALTER TABLE Search ADD CONSTRAINT FK_Search_idproject FOREIGN KEY (idproject) REFERENCES Project(idproject);

CREATE TABLE Takepart(
  idUsers INTEGER NOT NULL,
  idproject INTEGER NOT NULL
);

ALTER TABLE Takepart ADD PRIMARY KEY (idUsers,idproject);
ALTER TABLE Takepart ADD CONSTRAINT FK_Takepart_idUsers FOREIGN KEY (idUsers) REFERENCES Users(idUsers);
ALTER TABLE Takepart ADD CONSTRAINT FK_Takepart_idproject FOREIGN KEY (idproject) REFERENCES Project(idproject);

CREATE TABLE Follow(
idUserFollow INTEGER NOT NULL,
idUserFollowed INTEGER NOT NULL
);

ALTER TABLE Follow ADD PRIMARY KEY (idUserFollow,idUserFollowed);
ALTER TABLE Follow ADD CONSTRAINT FK_iduserFollow FOREIGN KEY (idUserFollow) REFERENCES Users(idUsers);
ALTER TABLE Follow ADD CONSTRAINT FK_iduserFollowed FOREIGN KEY (idUserFollowed) REFERENCES Users(idUsers);



