/*!

=========================================================
* Argon Design System React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-design-system-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-design-system-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from 'react';
// reactstrap components
import {Button, Card, Col, Container, Input, Row} from 'reactstrap';

import '../../assets/css/profile.css';
// core components
import SimpleFooter from 'components/Footers/SimpleFooter.jsx';
import SideBar from '../../components/Navbars/SideBar';
import MainNavbar from '../../components/Navbars/MainNavbar';
import {Link} from "react-router-dom";
import Validate from "../../components/Security/Validate";
import AddDomain from "../../components/Form/AddDomain";

class ModificationProfile extends React.Component {

    constructor() {
        super();
        this.state = {
            job: "",
            nom: "",
            prenom: "",
            mail: "",
            numero: "",
            domaine: "",
            idjob: "",
            nomjob: ""
        };
    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };


    removeAccount(event) {
        console.log("Compte supprimé !")
    }

    async componentDidMount() {
        setTimeout(() => {
            fetch(`http://wesearchgroup-preprod.herokuapp.com/user/${localStorage.getItem('iduser_wesearch')}`) //token de connexion
                .then(response => response.json())
                .then(data => {
                    this.setState({
                        nom: data.nom,
                        prenom: data.prenom,
                        mail: data.mail,
                        numero: data.numero,
                        nomjob: data.job
                    });
                })
        }, 1000);
    }

    render() {
        const {nom, prenom, mail, numero, idjob} = this.state;
        return (
            <>
                <Validate/>
                <MainNavbar/>
                <SideBar
                    center={
                        <>
                            <Container>
                                <div className="descriptionprofile px-0">
                                    <Card className="card-profile shadow ">
                                        <Row className="align-self-end mr-1">
                                            <Link
                                                onClick={(e) => this.updateProfile(e)}
                                            >
                                                <Button
                                                    color="primary"
                                                    size="sm"
                                                >
                                                    Enregistrer mon profil
                                                </Button>
                                            </Link>

                                            <span> &nbsp;&nbsp;&nbsp;   </span>

                                            <Link
                                                onClick={(e) => this.removeAccount(e)}
                                            >
                                                <Button
                                                    color="primary"
                                                    size="sm"
                                                >Supprimer mon profil
                                                </Button>
                                            </Link>

                                        </Row>
                                        <Row>

                                            <Col lg="3">
                                                <div className="text-md-center">
                                                    <img
                                                        alt="..."
                                                        className="float rounded-circle"
                                                        src="https://api.adorable.io/avatars/285/picture.png"
                                                        style={{width: '150px', height: '150px'}}
                                                    />
                                                </div>
                                            </Col>
                                            <Col>
                                                <br/>
                                                <br/>
                                                <br/>
                                                <input
                                                    name="nom"
                                                    type="text"
                                                    placeholder="Nom"
                                                    value={nom}
                                                    onChange={this.handleChange}
                                                />
                                            </Col>
                                            <Col>
                                                <br/>
                                                <br/>
                                                <br/>
                                                <input
                                                    name="prenom"
                                                    type="text"
                                                    placeholder="prenom"
                                                    value={prenom}
                                                    onChange={this.handleChange}
                                                />

                                            </Col>
                                        </Row>
                                        <div className="text-center">
                                            <div> Contact</div>
                                            <Row>
                                                <Col>
                                                    <form
                                                        className="text-center"
                                                        onSubmit={this.handleSubmit}>
                                                        <label htmlFor="mail"></label>
                                                        <input
                                                            type="text"
                                                            placeholder="Mail"
                                                            name="mail"
                                                            value={mail}
                                                            onChange={this.handleChange}
                                                        />
                                                    </form>
                                                </Col>
                                                <Col>
                                                    <form
                                                        className="text-center"
                                                        onSubmit={this.handleSubmit}>
                                                        <label htmlFor="numero"></label>
                                                        <input
                                                            type="text"
                                                            name="numero"
                                                            placeholder="Numero"
                                                            value={numero}
                                                            onChange={this.handleChange}/>
                                                    </form>
                                                </Col>
                                            </Row>
                                            <br/>
                                            <br/>
                                            <br/>
                                            <Row className="align-content-center">

                                                <Col>
                                                    <form className="text-center">
                                                        <Input
                                                            type="select"
                                                            name="idjob"
                                                            value={idjob}
                                                            onChange={this.handleChange}>
                                                            <option value="0">Ingénieur de projet</option>
                                                            <option value="1">Etudiant</option>
                                                            <option value="2">Doctorant/Chercheur</option>
                                                            <option value="3">Professeur/Chercheur</option>
                                                            <option value="4">Maître de conférence/Chercheur</option>
                                                            <option value="5">Chercheur</option>
                                                            <option value="6">Ingénieur de recherche</option>
                                                            <option defaultValue="3"> {this.state.nomjob} </option>
                                                        </Input>
                                                    </form>
                                                </Col>
                                                <Col>
                                                    <AddDomain to={'domain'}/>
                                                </Col>

                                            </Row>


                                            <div>
                                                <a className="contact" href="page pro"
                                                   onClick={e => e.preventDefault()}> </a>
                                            </div>
                                            <br/>
                                            <br/>
                                            <br/>
                                            <br/>
                                            <br/>
                                            <br/>
                                        </div>
                                    </Card>
                                </div>
                            </Container>
                        </>
                    }
                />
                <SimpleFooter/>
            </>
        );
    }

    updateProfile = event => {
        console.log(this.state.nom);
        fetch("https://wesearchgroup-preprod.herokuapp.com/user", {
            method: 'PUT',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idusers: localStorage.getItem('iduser_wesearch'),
                idlab: 1, //this.state.idlab,
                nom: this.state.nom,
                prenom: this.state.prenom,
                mail: this.state.mail,
                numero: this.state.numero,
                idjob: this.state.idjob
            })
        }).then(response => this.props.history.push(`/profile?q=${localStorage.getItem('iduser_wesearch')}`));

    }
}

export default ModificationProfile;