/*!

=========================================================
* Argon Design System React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-design-system-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-design-system-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import ReactDatetime from "react-datetime";
// reactstrap components
import {Button, Card, Container, Form, FormGroup, Input, InputGroup, InputGroupAddon, InputGroupText} from "reactstrap";
import MainNavbar from "../../components/Navbars/MainNavbar";
import SideBar from "../../components/Navbars/SideBar";
import SimpleFooter from "../../components/Footers/SimpleFooter";
import Validate from "../../components/Security/Validate";

class RegisterProject extends React.Component {
    constructor() {
        super();
        this.state = {
            titleProject: "",
            descProject: "",
            duration: ""
        }
    }

    render() {
        const {titleProject, descProject, duration} = this.state;
        return (
            <>
                <Validate/>
                <MainNavbar/>
                <SideBar
                    center={
                        <>
                            <Container>
                                <Card className="card-profile shadow ">

                                    <div className=" text-darker text-center">
                                        <br/>
                                        <h3> Création de projet </h3>
                                    </div>

                                    <Form className="align-self-md-center w-75">
                                        <FormGroup>
                                            <InputGroup className="input-group-alternative mb-3">
                                                <InputGroupAddon addonType="prepend">
                                                    <InputGroupText>
                                                        <i className="ni ni-circle-08"/>
                                                    </InputGroupText>
                                                </InputGroupAddon>
                                                <Input placeholder="Titre"
                                                       type="text" id="titleProject"
                                                       value={titleProject}
                                                       onChange={this.handleChange}
                                                />
                                            </InputGroup>
                                        </FormGroup>

                                        <FormGroup>
                                            <InputGroup className="input-group-alternative mb-3">
                                                <InputGroupAddon addonType="prepend">
                                                    <InputGroupText>
                                                        <i className="ni ni-email-83"/>
                                                    </InputGroupText>
                                                </InputGroupAddon>
                                                <Input placeholder="Décrivez votre projet" type="text"
                                                       value={descProject}
                                                       id="descProject"
                                                       onChange={this.handleChange}
                                                />
                                            </InputGroup>
                                        </FormGroup>


                                        <FormGroup>
                                            <InputGroup className="input-group-alternative">
                                                <InputGroupAddon addonType="prepend">
                                                    <InputGroupText>
                                                        <i className="ni ni-calendar-grid-58"/>
                                                    </InputGroupText>
                                                </InputGroupAddon>
                                                <ReactDatetime
                                                    inputProps={{
                                                        placeholder: "Date de début"

                                                    }}
                                                    value={this.state.duration}
                                                    id="duration"
                                                    timeFormat={false}
                                                />
                                            </InputGroup>
                                        </FormGroup>


                                        <div className="text-center">
                                            <Button
                                                className="mt-4"
                                                color="primary"
                                                type="button"
                                                onClick={this.addProject}
                                            >Créer le projet
                                            </Button>
                                        </div>
                                        <div>
                                            <br/>
                                        </div>
                                    </Form>
                                </Card>

                            </Container>
                        </>
                    }
                />
                <SimpleFooter/>
            </>
        );
    }

    handleChange = event => {

        this.setState({
            [event.target.id]: event.target.value
        });
    };


    addProject = event => {
        event.preventDefault();
        console.log(this.state.duration);
        fetch("https://wesearchgroup-preprod.herokuapp.com/project", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idUsers: localStorage.getItem("iduser_wesearch"),
                title: this.state.titleProject,
                description: this.state.descProject,
                startingdate: Date.now()
            })
        }).then(response =>response.json())
            .then(data=> {
                console.log(data);
                this.props.history.push(`/project?q=${data.idproject }`)

            });
    }

}

export default RegisterProject;
