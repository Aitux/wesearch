import React from "react";
// reactstrap components
import {Button, Card, Col, Container, Row} from "reactstrap";

import "../../assets/css/project.css";
// core components
import SimpleFooter from "components/Footers/SimpleFooter.jsx";
import MainNavbar from "../../components/Navbars/MainNavbar";
import SideBar from "../../components/Navbars/SideBar";
import PostHandler from "../../components/Post/PostHandler";
import {Link} from "react-router-dom";
import queryString from "query-string";
import Validate from "../../components/Security/Validate";
import AddItem from "../../components/Form/AddItem";
import ListHandler from "../../components/Form/ListHandler";
import AtomicDomain from "../../components/Domain/AtomicDomain";

class Project extends React.Component {
    constructor() {
        super();
        this.state = {
            idUser: 1,
            idProject: 1,
            user: {},
            project: {},
            listCom: [],
            takePart: "Participer",
            funcTakePart: this.joinProject,
            outlined: false,
            display: true,
            listPropose: [],
            listSearch: []
        };
    }

    async componentDidMount() {
        const values = queryString.parse(this.props.location.search);

        setTimeout(() => {
            fetch(`https://wesearchgroup-preprod.herokuapp.com/project/${values.q}`) //token de connexion
                .then(response => response.json())
                .then(data => {
                        this.setState({
                            project: data
                        });
                        console.log(data)
                    }
                ).then(_ => {
                const id = this.state.project.idUsers;
                this.setState({
                    display: id == localStorage.getItem('iduser_wesearch')
                })
            })
        }, 1000);


        fetch(`https://wesearchgroup-preprod.herokuapp.com/takepart/${localStorage.getItem("iduser_wesearch")}/${values.q}`)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    outlined: data["response"],
                    funcTakePart: (!data["response"] ? this.joinProject : this.leaveProject),
                    takePart: (!data["response"] ? "Participer" : "Quitter")
                })
            })

        fetch(`https://wesearchgroup-preprod.herokuapp.com/searching/${values.q}`)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    listSearch: data
                })
            })

        fetch(`https://wesearchgroup-preprod.herokuapp.com/propose/${values.q}`)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    listPropose: data
                })
            })
    }

    render() {
        const values = queryString.parse(this.props.location.search);
        let arrDom = [];
        console.log(this.state.project.domain);
        return (
            <>
                <Validate/>
                <MainNavbar/>
                <main className="project" ref="main">
                    <SideBar
                        center={
                            <>
                                <Container>
                                    <Card className=" card-profile shadow">
                                        <div className="cardproject">
                                            <Row className="d-flex">
                                                <Link
                                                    to={`/modificationProject?q=${values.q}`}
                                                    className="ml-auto">
                                                    <Button
                                                        className={this.state.display ? 'hidden' : ''}
                                                        color="primary"
                                                        size="sm"
                                                    >
                                                        <i className="ni ni-settings-gear-65"/>
                                                    </Button>
                                                </Link>
                                            </Row>
                                            <Container>
                                                <Row className="d-flex">
                                                    <h3 className="m-auto">
                                                        {this.state.project.title}
                                                    </h3>
                                                </Row>


                                                <Row className="d-flex">
                                                    <p className="m-auto text-center">
                                                        {this.state.project.description}
                                                    </p>
                                                </Row>
                                            </Container>
                                            <Row className="cardproject">
                                                <Col>
                                                    <Link
                                                        to={`/profile?q=${this.state.project.creator && this.state.project.creator.idUsers}`}>
                                                        <div className="text-center">
                                                            <h5>Contact </h5>
                                                            <Row className="d-flex">
                                                            <span className="m-auto text-center">
                                                                {this.state.project.creator && `${this.state.project.creator.nom} ${this.state.project.creator.prenom}`}
                                                            </span>

                                                            </Row>
                                                            <Row className="d-flex">
                                                            <span className="m-auto text-center">
                                                                {this.state.project.creator && `${this.state.project.creator.job}`}
                                                            </span>
                                                            </Row>
                                                            <Row className="d-flex">
                                                            <span className="m-auto text-center">
                                                                {this.state.project.creator && `Laboratoire ${this.state.project.creator.labo}`}
                                                            </span>
                                                            </Row>
                                                            <Row className="d-flex">

                                                            <span className="m-auto text-center">
                                                                {this.state.project.creator && `${this.state.project.creator.mail}`}
                                                            </span>
                                                            </Row>
                                                        </div>
                                                    </Link>
                                                </Col>

                                                <Col>
                                                    <div className="text-center">
                                                        <h5>Domaine</h5>
                                                        <div>
                                                            <AtomicDomain label={this.state.project.domain || 'Informatique' }/>
                                                        </div>
                                                    </div>
                                                </Col>
                                            </Row>
                                            <Container>
                                                <Row className="d-flex">
                                                    <div className="buttonabo ml-auto">
                                                        <Button
                                                            color="primary"
                                                            outline={this.state.outlined}
                                                            size="sm"
                                                            onClick={this.state.funcTakePart}
                                                        >
                                                            {this.state.takePart}
                                                        </Button>
                                                    </div>
                                                </Row>
                                            </Container>

                                            <Row>
                                                <Col>
                                                    <div className="mt-5 pt-5 border-top text-center">
                                                        <Row className="justify-content-center">
                                                            <Col lg="9">
                                                                <h5>
                                                                    Le projet propose:
                                                                </h5>
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                </Col>
                                                <Col>
                                                    <div className="mt-5 pt-5 border-top text-center">
                                                        <Row className="justify-content-center">
                                                            <Col lg="9">
                                                                <h5>
                                                                    Le projet a besoin de:
                                                                </h5>
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col>
                                                    <div className="py-5 text-center">
                                                        <Row className="justify-content-center">
                                                            <Col lg="9">
                                                                <ul>
                                                                    <ListHandler list={this.state.listPropose}
                                                                                 handler={this.handleDeletePropose}/>
                                                                </ul>
                                                                <AddItem to={'propose'} id={values.q}
                                                                         iduser={this.state.project.creator && this.state.project.creator.idUsers}
                                                                         handler={this.handlerPropose}/>
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                </Col>
                                                <Col>
                                                    <div className="py-5 text-center">
                                                        <Row className="justify-content-center">
                                                            <Col lg="9">
                                                                <ul>
                                                                    <ListHandler list={this.state.listSearch}
                                                                                 handler={this.handleDeleteSearch}/>
                                                                </ul>
                                                                <AddItem to={'searching'} id={values.q}
                                                                         iduser={this.state.project.creator && this.state.project.creator.idUsers}
                                                                         handler={this.handlerSearch}/>
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                </Col>
                                            </Row>
                                        </div>
                                    </Card>

                                    <Card className="card-profile shadow ">
                                        <div className="posts px-4">
                                            <div>
                                                <medium>Les derniers posts</medium>
                                            </div>
                                            <PostHandler towo={5} listPost={this.state.listCom}/>
                                        </div>
                                    </Card>
                                </Container>
                            </>
                        }
                    />
                    <SimpleFooter/>
                </main>
            </>
        );
    }

    handleDeleteSearch = (item) => {
        this.setState(state => {
            const listSearch = state.listSearch.filter(function (value, index, arr) {
                return value.id !== item.id
            });
            return {
                listSearch
            }
        })
        fetch(`https://wesearchgroup-preprod.herokuapp.com/searching/${item.id}`, {
            method: 'DELETE'
        });
    };

    handleDeletePropose = (item) => {
        this.setState(state => {
            const listPropose = state.listPropose.filter(function (value, index, arr) {
                return value.id !== item.id
            });
            return {
                listPropose
            }
        })

        fetch(`https://wesearchgroup-preprod.herokuapp.com/propose/${item.id}`, {
            method: 'DELETE'
        });
    };

    handlerSearch = (item) => {
        console.log(item);
        this.setState(state => {
            const listSearch = state.listSearch.concat(item);
            return {
                listSearch
            };
        });
    };

    handlerPropose = (item) => {
        console.log(item);
        this.setState(state => {
            const listPropose = state.listPropose.concat(item);
            return {
                listPropose
            }
        });
    };

    joinProject = event => {
        event.preventDefault();
        const values = queryString.parse(this.props.location.search);
        fetch(`https://wesearchgroup-preprod.herokuapp.com/takepart`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idUsers: localStorage.getItem("iduser_wesearch"),
                idproject: values.q
            })

        })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({takePart: "Quitter", outlined: true})
            });
    };

    leaveProject = event => {
        event.preventDefault();
        const values = queryString.parse(this.props.location.search);
        fetch(`https://wesearchgroup-preprod.herokuapp.com/takepart`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idUsers: localStorage.getItem("iduser_wesearch"),
                idproject: values.q
            })

        })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({takePart: "Participer", outlined: false})
            });
    }


}

export default Project;
