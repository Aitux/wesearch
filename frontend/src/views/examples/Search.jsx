import React from 'react';
// core components
import SimpleFooter from 'components/Footers/SimpleFooter.jsx';
import SideBar from '../../components/Navbars/SideBar';
import MainNavbar from '../../components/Navbars/MainNavbar';
import Validate from "../../components/Security/Validate";
import queryString from "query-string";
import SearchHandler from "../../components/Search/SearchHandler";

// reactstrap components

class Search extends React.Component {
	constructor() {
		super();
		this.state = {
			usersList: [],
			projectsList: []
		}
	}

	componentDidMount() {
		const values = queryString.parse(this.props.location.search);
		setTimeout(() => {
			fetch("https://wesearchgroup-preprod.herokuapp.com/search", {
				method: 'POST',
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					query: values.q
				})
			}).then(response => response.json()).then(json => {
				this.setState({
					usersList: json['users'],
					projectsList: json['projects']
				})
			})
		}, 1000);
	}


	render() {
		return (
			<>
				<MainNavbar/>
				<Validate/>
				<SideBar
					center={
						<>
							{
							<SearchHandler projects={this.state.projectsList} users={this.state.usersList}/>
							}
						</>
					}
				>
					<SimpleFooter/>
				</SideBar>
			</>
		);
	}
}

export default Search;
