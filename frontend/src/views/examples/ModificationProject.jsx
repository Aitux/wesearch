import React from "react";
// reactstrap components
import {Button, Card, Col, Container, Row} from "reactstrap";

import "../../assets/css/project.css";
// core components
import SimpleFooter from "components/Footers/SimpleFooter.jsx";
import MainNavbar from "../../components/Navbars/MainNavbar";
import SideBar from "../../components/Navbars/SideBar";
import Validate from "../../components/Security/Validate";
import queryString from "query-string";
import AddDomain from "../../components/Form/AddDomain";

class ModificationProject extends React.Component {
    constructor() {
        super();
        this.state = {
            title: "",
            idUser: 1,
            idProject: 1,
            listCom: [],
            description: "",
            mail: "",
            idDomain: ""
        };
        this.removeProject = this.removeProject.bind(this);
    }


    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    async removeProject(event) {
        console.log("Projet supprimé !");
        if (window.confirm(`On envoie ${this.state.title} vers le néant ?`)) {
            await this.fetch('delete', `http://wesearchgroup-preprod.herokuapp.com/project/${this.state.idProject}`);
        }
    }


    async componentDidMount() {
        const values = queryString.parse(this.props.location.search);
        this.setState({idProject: values.q});
        setTimeout(() => {
            fetch(`http://wesearchgroup-preprod.herokuapp.com/project/${this.state.idProject}`) //token de connexion
                .then(response => response.json())
                .then(data =>
                    this.setState({
                        title: data.title,
                        idDomain: data.idDomain,
                        description: data.description,
                        mail: data.mail
                    })
                )
        }, 1000);
    }

    render() {
        const {title, description, domaine} = this.state;
        const values = queryString.parse(this.props.location.search);

        return (
            <>
                <Validate/>
                <MainNavbar/>
                <main className="profile" ref="main">

                    <SideBar
                        center={
                            <>
                                <Container>
                                    <Card className=" card-profile shadow">
                                        <div className="cardproject">
                                            <Row className="align-self-end mt-3">
                                                <Col className="text-left ">
                                                    <div className="">
                                                        <Button
                                                            color="primary"

                                                            onClick={this.updateProject}
                                                            size="sm"
                                                        >
                                                            Enregistrer mon project
                                                        </Button>
                                                        <div><br/></div>
                                                    </div>
                                                </Col>
                                                <Col className="text-right">
                                                    <Button
                                                        color="primary"
                                                        onClick={this.removeProject}
                                                        size="sm"
                                                    >
                                                        Supprimer mon project
                                                    </Button>
                                                </Col>
                                            </Row>
                                            <div className="text-center mt-2">
                                                <h3>
                                                    <form
                                                        className="text-center"
                                                        onSubmit={this.handleSubmit}>
                                                        <label htmlFor="title"></label>
                                                        <input
                                                            name="title"
                                                            type="text"
                                                            placeholder="title"
                                                            onChange={this.handleChange}
                                                            value={title}
                                                        />
                                                    </form>
                                                </h3>
                                            </div>

                                            <div className="">
                                                <p>
                                                    <form
                                                        className="text-center"
                                                        onSubmit={this.handleSubmit}>
                                                        <label htmlFor="description"></label>
                                                        <input
                                                            name="description"
                                                            type="text"
                                                            placeholder="Description"
                                                            onChange={this.handleChange}
                                                            value={description}
                                                        />
                                                    </form>
                                                </p>
                                            </div>

                                            <Row>
                                                <Col>
                                                    <AddDomain to={`domain/${values.q}/`} unique={true}/>
                                                </Col>
                                            </Row>
                                        </div>
                                    </Card>
                                </Container>
                            </>
                        }
                    />
                    <SimpleFooter/>
                </main>
            </>
        );
    }

    updateProject = event => {
        const values = queryString.parse(this.props.location.search);
        console.log(this.state.nom);
        fetch("https://wesearchgroup-preprod.herokuapp.com/project", {
            method: 'PUT',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idproject: values.q,
                title: this.state.title,
                description: this.state.description,
                mail: this.state.mail,
                idDomain : this.state.idDomain
            })
        }).then(response => this.props.history.push(`/project?q=${values.q}`));

    }
}

export default ModificationProject;
