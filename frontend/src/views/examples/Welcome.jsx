import React from 'react';
import '../../assets/css/welcome.css';
// core components
import SimpleFooter from 'components/Footers/SimpleFooter.jsx';
import SideBar from '../../components/Navbars/SideBar';
import PostHandler from '../../components/Post/PostHandler';
import Validate from "../../components/Security/Validate";
import {Container} from "reactstrap";
import PostPoster from "../../components/Post/PostPoster";
import MainNavbar from "../../components/Navbars/MainNavbar";

// reactstrap components

class Welcome extends React.Component {
    constructor() {
        super();
        this.state = {
            listPost: {},
        }
    }

    async componentDidMount() {
		setTimeout(() => {
            fetch(`https://wesearchgroup-preprod.herokuapp.com/user/${localStorage.getItem('iduser_wesearch')}/feed`) //token de connexion
				.then(response => response.json())
				.then(data => {
					this.setState({
						listPost: data
					});
				})
		}, 1000);
    }

    render() {
        return (
            <>
                <Validate/>
                <MainNavbar/>
                <SideBar
                    center={
                        <>
                            <Container>
                                <PostPoster/>
                                {

                                    <PostHandler listPost={this.state.listPost}/>
                                }
                            </Container>
                        </>
                    }
                >
                    <SimpleFooter/>
                </SideBar>
            </>
        );
    }
}

export default Welcome;
