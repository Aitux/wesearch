/*!

=========================================================
* Argon Design System React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-design-system-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-design-system-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import "assets/css/register.css"
// reactstrap components
import {
    Button,
    Card,
    CardBody,
    Col,
    Container,
    Form,
    FormGroup,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Row
} from "reactstrap";
import {Link} from "react-router-dom";

class Register extends React.Component {
    constructor() {
        super();
        this.state = {
            nom: "",
            prenom: "",
            password: "",
            mail: "",
            numero: "",
            poste:""
        }
    }


    render() {
        const {nom, prenom, password, mail, numero, poste} = this.state;
        return (
            <>
                <main ref="main">

                    <section className="section section-shaped section-lg">
                        <div className="shape shape-style-1 bg-gradient-default">
                            <span/>
                            <span/>
                            <span/>
                            <span/>
                            <span/>
                            <span/>
                            <span/>
                            <span/>
                        </div>
                        <Row className="justify-content-center">
                            <img
                                src={require("assets/img/brand/logo_transparent.png")
                                }
                                className="logo"
                                alt="..."
                            >
                            </img>
                        </Row>
                        <Container className="pt-lg-md">
                            <Row className="justify-content-center">
                                <Col lg="5">
                                    <Card className="bg-secondary shadow border-0">
                                        <CardBody className="px-lg-5 py-lg-5">
                                            <div className="text-center text-muted mb-4">
                                                <small>Inscrivez-vous</small>
                                            </div>
                                            <Form role="form">
                                                <FormGroup>
                                                    <InputGroup className="input-group-alternative mb-3">
                                                        <InputGroupAddon addonType="prepend">
                                                            <InputGroupText>
                                                                <i className="ni ni-circle-08"/>
                                                            </InputGroupText>
                                                        </InputGroupAddon>
                                                        <Input placeholder="Nom" type="text" value={nom}
                                                               id="nom" onChange={this.handleChange}
                                                        />
                                                        <Input placeholder="Prénom" type="text" value={prenom}
                                                               id="prenom" onChange={this.handleChange}/>
                                                    </InputGroup>
                                                </FormGroup>
                                                <FormGroup>
                                                    <InputGroup className="input-group-alternative mb-3">
                                                        <InputGroupAddon addonType="prepend">
                                                            <InputGroupText>
                                                                <i className="ni ni-email-83"/>
                                                            </InputGroupText>
                                                        </InputGroupAddon>
                                                        <Input placeholder="prenom.nom@univ-lille.fr" type="email"
                                                               value={mail}
                                                               id="mail" onChange={this.handleChange}/>
                                                    </InputGroup>
                                                </FormGroup>

                                                <FormGroup>
                                                    <InputGroup className="input-group-alternative">
                                                        <InputGroupAddon addonType="prepend">
                                                            <InputGroupText>
                                                                <i className="ni ni-lock-circle-open"/>
                                                            </InputGroupText>
                                                        </InputGroupAddon>
                                                        <Input
                                                            placeholder="Mot de passe"
                                                            type="password"
                                                            value={password}
                                                            autoComplete="off"
                                                            id="password"
                                                            onChange={this.handleChange}
                                                        />
                                                    </InputGroup>
                                                </FormGroup>
                                                <FormGroup>
                                                    <InputGroup className="input-group-alternative">
                                                        <InputGroupAddon addonType="prepend">
                                                            <InputGroupText>
                                                                <i className="ni ni-lock-circle-open"/>
                                                            </InputGroupText>
                                                        </InputGroupAddon>
                                                        <Input
                                                            placeholder="Confirmation mot de passe"
                                                            type="password"
                                                            autoComplete="off"
                                                            id="confirmPwd"
                                                        />
                                                    </InputGroup>
                                                </FormGroup>
                                                <FormGroup>
                                                    <InputGroup className="input-group-alternative mb-3">
                                                        <InputGroupAddon addonType="prepend">
                                                            <InputGroupText>
                                                                <i className="ni ni-briefcase-24"/>
                                                            </InputGroupText>
                                                        </InputGroupAddon>

                                                        <Input
                                                            type="select"
                                                            value={poste}
                                                            id="poste" onChange={this.handleChange}>
                                                            <option value="0">Ingénieur de projet</option>
                                                            <option value="1">Etudiant</option>
                                                            <option value="2">Doctorant/Chercheur</option>
                                                            <option selected value="3">Professeur/Chercheur</option>
                                                            <option value="4">Maître de conférence/Chercheur</option>
                                                            <option value="5">Chercheur</option>
                                                            <option value="6">Ingénieur de recherche</option>

                                                        </Input>
                                                    </InputGroup>
                                                </FormGroup>

                                                <FormGroup>
                                                    <InputGroup className="input-group-alternative">
                                                        <InputGroupAddon addonType="prepend">
                                                            <InputGroupText>
                                                                <i className="ni ni-mobile-button"/>
                                                            </InputGroupText>
                                                        </InputGroupAddon>
                                                        <Input
                                                            placeholder="Numéro de téléphone"
                                                            type="text"
                                                            value={numero}
                                                            id="numero"
                                                            onChange={this.handleChange}
                                                        />
                                                    </InputGroup>
                                                </FormGroup>
                                                <div className="text-center">
                                                    <Link
                                                        color="primary"
                                                        // to="/welcome"
                                                        onClick={this.compil}
                                                    >
                                                    <Button
                                                        className="mt-4"
                                                        type="button">

                                                            Créer un compte

                                                    </Button>
                                                    </Link>
                                                </div>
                                                <Row className="mt-3">
                                                    <Col xs="6">
                                                        <Link
                                                            className="text-light"
                                                            to="/login"
                                                        >
                                                            <span>Retour à l'accueil</span>
                                                        </Link>
                                                    </Col>
                                                </Row>
                                            </Form>
                                        </CardBody>
                                    </Card>
                                </Col>
                            </Row>
                        </Container>
                    </section>
                </main>
            </>
        );
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };


    compil = event => {
        event.preventDefault();
        console.log(this.state.poste);
        fetch("https://wesearchgroup-preprod.herokuapp.com/user", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idlab: 1,
                idjob:this.state.poste,
                nom: this.state.nom,
                prenom: this.state.prenom,
                mail: this.state.mail,
                password: this.state.password,
                numero: this.state.numero,
                idrole: 1
            })
        }).then(response => response.json())
            .then(data => {
                console.log(data);
                this.props.history.push(`/`)
            });
    }
}
export default Register;
