import React from 'react';
import queryString from 'query-string'
// reactstrap components
import {Button, Card, Col, Container, Row} from 'reactstrap';

import '../../assets/css/profile.css';
// core components
import SimpleFooter from 'components/Footers/SimpleFooter.jsx';
import SideBar from '../../components/Navbars/SideBar';
import '../../assets/css/postposter.css'
import MainNavbar from '../../components/Navbars/MainNavbar';
import PostHandler from '../../components/Post/PostHandler';
import {Link} from "react-router-dom";
import ProjectHandler from "../../components/Project/ProjectHandler";
import HandlerDomain from "../../components/Domain/HandlerDomain";


class Profile extends React.Component {

    constructor() {
        super();
        this.state = {
            loading: false,
            followed: "S'abonner",
            user: {},
            funcForBut: this.addFollow,
            project: {},
            outlined: false,
            connec: {},
            listProjects: [],
        };
    }

    async componentDidMount() {
        const values = queryString.parse(this.props.location.search);
        setTimeout(() => {

            fetch(`https://wesearchgroup-preprod.herokuapp.com/user/${values.q}/projects`)
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    this.setState({
                        listProjects: data
                    });
                })
        }, 1000);
        setTimeout(() => {
            fetch(`https://wesearchgroup-preprod.herokuapp.com/user/${values.q}/feed`) //token de connexion
                .then(response => response.json())
                .then(data => {
                    this.setState({
                        listPost: data
                    });
                })
        }, 1000);

        setTimeout(() => {
            fetch(`https://wesearchgroup-preprod.herokuapp.com/user/${values.q}`) //token de connexion
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    this.setState({
                        connec: data
                    });
                })
        }, 1000);

        fetch(`https://wesearchgroup-preprod.herokuapp.com/follow/${localStorage.getItem("iduser_wesearch")}/${values.q}`)
            .then(response => response.json())
            .then(data => {
                console.log(data["response"]);
                this.setState({
                    followed: (!data["response"] ? "S'abonner" : "Se désabonner"),
                    funcForBut: (!data["response"] ? this.addFollow : this.unfollow),
                    outlined: data["response"]
                });
            });

    }

    render() {
        const values = queryString.parse(this.props.location.search);

        return (
            <>
                <MainNavbar/>
                <SideBar
                    center={
                        <>
                            <Container>
                                <div className="descriptionprofile px-0">
                                    <Card className="card-profile shadow">
                                        <Row className="align-self-end mr-1">
                                            <Link
                                                to={`/modificationProfile?q=${values.q}`}>

                                                <Button
                                                    className={localStorage.getItem("iduser_wesearch") !== values.q ? 'btn btn-primary btn-sm hidden' : 'btn btn-primary btn-sm'}
                                                    color="primary"
                                                    size="sm"
                                                >
                                                    <i className="ni ni-settings-gear-65"/>
                                                </Button>
                                            </Link>
                                        </Row>
                                        <Row className="">
                                            <Col lg="5">
                                                <div className="text-md-center">
                                                    <img
                                                        alt="..."
                                                        className="float rounded-circle"
                                                        src="https://api.adorable.io/avatars/285/picture.png"
                                                        style={{width: '150px', height: '150px'}}
                                                    />
                                                </div>
                                            </Col>
                                            <Col lg="5">
                                                <div className="">
                                                    <br/>
                                                    <br/>
                                                    <br/>
                                                    <h3 className="name text-center">
                                                        <span>{this.state.connec.nom} {this.state.connec.prenom}</span>
                                                    </h3>
                                                    <h5 className="text-center">{this.state.connec.job}</h5>
                                                </div>
                                            </Col>
                                        </Row>
                                        <div className="text-center">
                                            <div> Contact</div>
                                            <Row>
                                                <Col>
                                                    <i className="ni ni-email-83 m-auto"/> <span
                                                    className="mt-auto">{this.state.connec.mail}</span>
                                                </Col>
                                                <Col>
                                                    <i className="ni ni-mobile-button m-auto"/>
                                                    <span>{this.state.connec.numero}</span>
                                                </Col>
                                            </Row>
                                            <div>
                                                <a className="contact" href="page pro"
                                                   onClick={e => e.preventDefault()}> </a>
                                            </div>
                                        </div>

                                        <div className="text-center ">
                                            Domaine(s)
                                            <p>
                                                <br/>
                                                {<HandlerDomain listDomains={this.state.connec.domains}/>}
                                            </p>
                                        </div>

                                        <Row>
                                            <Col className="text-center">
                                                <div>
                                                    <span> {this.state.connec.nbAbo} Abonnements</span>
                                                   <br/>
                                                   <br/>
                                                </div>
                                            </Col>
                                            <Col className="text-center">
                                                <div>
                                                    <Button
                                                        className={localStorage.getItem("iduser_wesearch") == values.q ? 'btn btn-primary btn-sm hidden' : 'btn btn-primary btn-sm'}
                                                        color="primary"
                                                        size="sm"
                                                        onClick={this.state.funcForBut}
                                                        outline={this.state.outlined}
                                                    >
                                                        {this.state.followed}
                                                    </Button>
                                                    <div><br/></div>
                                                </div>
                                            </Col>
                                        </Row>
                                    </Card>

                                </div>



                                <Card className="card-profile shadow ">
                                    <Container>
                                        <Row>
                                            <Col>
                                                <div className="projets text-center px-4">
                                                    <h5> Les projets</h5>
                                                </div>

                                                <div>
                                                    <ProjectHandler towo={2} listProjects={this.state.listProjects}
                                                                    lg={12}/>
                                                </div>
                                            </Col>

                                            <Col>
                                                <div className="posts text-center px-4">
                                                    <div>
                                                        <h5> Les derniers posts</h5>
                                                    </div>
                                                    <PostHandler towo={3} listPost={this.state.listPost} readOnly={true}/>
                                                </div>
                                            </Col>
                                        </Row>
                                    </Container>
                                </Card>

                            </Container>
                        </>
                    }

                />
                <SimpleFooter/>
            </>
        );
    }


    addFollow = (event) => {
        event.preventDefault();
        const values = queryString.parse(this.props.location.search);
        fetch(`https://wesearchgroup-preprod.herokuapp.com/follow`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                iduserfollow: localStorage.getItem("iduser_wesearch"),
                iduserfollowed: values.q
            })

        })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({followed: "Se désabonner", outlined: true})
            });
    };

    unfollow = (event) => {
        event.preventDefault();
        const values = queryString.parse(this.props.location.search);
        fetch(`https://wesearchgroup-preprod.herokuapp.com/follow`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                iduserfollow: localStorage.getItem("iduser_wesearch"),
                iduserfollowed: values.q
            })

        })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({followed: "S'abonner", outlined: false})
            });
    };

}

export default Profile;