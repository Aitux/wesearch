import React from 'react';
// core components
import SimpleFooter from 'components/Footers/SimpleFooter.jsx';
import SideBar from '../../components/Navbars/SideBar';
import MainNavbar from '../../components/Navbars/MainNavbar';
import Validate from "../../components/Security/Validate";
import FeedBackAlert from "../../components/Modal/FeedBackAlert";
import {Container} from "reactstrap";
import ProjectHandler from "../../components/Project/ProjectHandler";

// reactstrap components

class ListProjects extends React.Component {
    constructor() {
        super();
        this.state = {
            listProject: null,
        }

    }

    componentDidMount() {

            fetch(`https://wesearchgroup-preprod.herokuapp.com/user/${localStorage.getItem("iduser_wesearch")}/projects`) //token de connexion
                .then(response => response.json())
                .then(data => {
                    this.setState({
                        listProject: data
                    });
                });
    }


    render() {
        return (
            <>
                <Validate/>
                <MainNavbar/>
                <SideBar
                    center={
                        <>
                            <Container>
                                {
                                    <ProjectHandler listProjects={this.state.listProject}/>
                                }
                            </Container>
                        </>
                    }
                >
                    <SimpleFooter/>
                    <FeedBackAlert/>
                </SideBar>
            </>
        );
    }
}

export default ListProjects;
