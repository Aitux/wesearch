/*!

=========================================================
* Argon Design System React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-design-system-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-design-system-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from 'react';
import '../../assets/css/login.css';
import {Link} from "react-router-dom";
// reactstrap components
import {
    Alert,
    Button,
    Card,
    CardHeader,
    Col,
    Container,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Row
} from 'reactstrap';
import Input from "reactstrap/lib/Input";
import Form from "reactstrap/es/Form";
import FormGroup from "reactstrap/es/FormGroup";

// const API = "https://wesearchgroup-preprod.herokuapp.com/authorize";

// core components

class Login extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            email: "",
            password: ""
        };
    }


    render() {
        const {email, password} = this.state;

        return (
            <>
                <main ref="main">
                    <section className="section section-shaped section-lg h-100vh">
                        <div className="shape shape-style-1 bg-gradient-default ">
                            <span/>
                            <span/>
                            <span/>
                            <span/>
                            <span/>
                            <span/>
                            <span/>
                            <span/>
                        </div>
                        <Row className="justify-content-center">
                            <img
                                src={require('assets/img/brand/logo_transparent.png')
                                }
                                className="logo"
                                alt="..."
                            >
                            </img>
                        </Row>
                        <Row>
                            <Col>
                                <Container className="pt-lg-md">
                                    <Row className="justify-content-end">
                                        <Alert className="alert-white" fade={false}>

                                            <ul>
                                                <li>
                                            <span className="alert-inner--icon text-black-50">
                                    <i className="ni ni-atom"/>
                                    </span>{' '}
                                                    <span className="alert-inner--text text-black-50">Un nouveau moyen de faire émerger des projets <strong>pluridisciplinaires</strong>.</span>
                                                </li>
                                                <li></li>
                                                <li>
                                            <span className="alert-inner--icon text-black-50">
                                    <i className="ni ni-bulb-61"/>
                                    </span>{' '}
                                                    <span className="alert-inner--text text-black-50">
                                                      Restez informé des <strong>dernières nouveautés</strong> de la communauté scientifique.
									</span>
                                                </li>
                                                <li></li>
                                                <span className="alert-inner--icon text-black-50">
                                    <i className="ni ni-like-2"/>
                                    </span>{' '}
                                                <span className="alert-inner--text text-black-50">
                                      <strong>Trouvez</strong> les talents qu'il vous manque.
									</span></ul>
                                        </Alert>
                                    </Row>
                                </Container>
                            </Col>
                            <Col>
                                <Container className="pt-lg-md">
                                    <Row className="justify-content-start">
                                        <Col lg="5">
                                            <Card className="bg-secondary shadow border-0">
                                                <CardHeader className="bg-white pb-5 ">
                                                    <div className="text-muted text-center mb-3">
                                                        <small>Se connecter avec</small>
                                                    </div>
                                                    <div className="text-center">
                                                        <Button
                                                            disabled
                                                            className="btn-neutral btn-icon"
                                                            color="default"
                                                            href="#pablo"
                                                            onClick={e => e.preventDefault()}
                                                        >
                                                            <span className="btn-inner--icon ">
                                                                <img
                                                                    alt="..."
                                                                    src={require('assets/img/icons/common/twitter.svg')} //CHANGER L'IMAGE
                                                                />
                                                            </span>
                                                            <span className="btn-inner--text">Twitter</span>

                                                        </Button>
                                                        <Button
                                                            disabled
                                                            className="btn-neutral btn-icon "
                                                            color="default"
                                                            href="#pablo"
                                                            onClick={e => e.preventDefault()}
                                                        >
                                                            <span className="btn-inner--icon ">
                                                                <img
                                                                    alt="..."
                                                                    src={require('assets/img/icons/common/google.svg')}
                                                                />
                                                            </span>
                                                            <span className="btn-inner--text">Google</span>
                                                        </Button>

                                                    </div>
                                                </CardHeader>

                                                <Form
                                                    className="text-center"
                                                    onSubmit={this.handleSubmit}>
                                                    <Col>
                                                        <FormGroup>
                                                            <InputGroup className="mb-4">
                                                                <InputGroupAddon addonType="prepend">
                                                                    <InputGroupText>
                                                                        <i className="ni ni-circle-08"/>
                                                                    </InputGroupText>
                                                                </InputGroupAddon>
                                                                <Input placeholder="Email" type="text"
                                                                       name="email" value={email}
                                                                       onChange={this.handleChange}/>
                                                            </InputGroup>
                                                        </FormGroup>
                                                    </Col>
                                                    <Col>
                                                        <FormGroup>
                                                            <InputGroup className="mb-4">
                                                                <InputGroupAddon addonType="prepend">
                                                                    <InputGroupText>
                                                                        <i className="ni ni-lock-circle-open"/>
                                                                    </InputGroupText>
                                                                </InputGroupAddon>
                                                                <Input placeholder="Password" type="password"
                                                                       name="password" value={password}
                                                                       onChange={this.handleChange}/>
                                                            </InputGroup>
                                                        </FormGroup>
                                                    </Col>
                                                    <p></p>
                                                    <Button
                                                        className="btn-neutral btn-icon ml-1 "
                                                        type="submit"
                                                        onClick={this.connection}
                                                    >
                                                        Se connecter
                                                    </Button>
                                                    <p></p>
                                                </Form>
                                            </Card>
                                            <Row className="mt-3">
                                                <Col xs="6">
                                                    <Link
                                                        className="text-light"

                                                        to="/passwordForgotten"
                                                    >
                                                        <small>Mot de passe oublié</small>
                                                    </Link>
                                                </Col>
                                                <Col className="text-right" xs="6">
                                                    <Link
                                                        className="text-light"
                                                        to="/register"
                                                    >
                                                        <small>Créer un compte</small>
                                                    </Link>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Container>
                            </Col>
                        </Row>
                    </section>
                </main>
            </>
        );
    }


    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    connection = event => {
        event.preventDefault();
        fetch("https://wesearchgroup-preprod.herokuapp.com/authorize", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                mail: this.state.email,
                password: this.state.password
            })
        }).then(response => response.json()).then(json => {
            console.log(json);
            console.log(json["response"]);
            if ((json["response"] !== null || json["response"] !== undefined) && json["response"] !== 'invalid') {
                localStorage.setItem("token_wesearch", json["response"]);
                localStorage.setItem("iduser_wesearch", json["idUser"]);
                this.props.history.push('/welcome');
            }
        })
    }


}

export default Login;
