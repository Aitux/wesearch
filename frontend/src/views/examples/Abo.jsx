import React from 'react';
import '../../assets/css/welcome.css';
// core components
import SimpleFooter from 'components/Footers/SimpleFooter.jsx';
import SideBar from '../../components/Navbars/SideBar';
import MainNavbar from '../../components/Navbars/MainNavbar';
import Validate from "../../components/Security/Validate";
import FeedBackAlert from "../../components/Modal/FeedBackAlert";
import FollowerHandler from "../../components/Follow/FollowerHandler";

// reactstrap components

class Abo extends React.Component {
    constructor() {
        super();
        this.state = {
            listFriends: null,
        }
    }

    componentDidMount() {
            fetch(`https://wesearchgroup-preprod.herokuapp.com/user/${localStorage.getItem('iduser_wesearch')}/friends`) //token de connexion
                .then(response => response.json())
                .then(data => {
                    this.setState({
                        listFriends: data
                    });
                })

    }


    render() {
        return (
            <>
                <Validate/>
                <MainNavbar/>
                <SideBar
                    center={
                        <>
                            <div>
                                {
                                    <FollowerHandler listProfile={this.state.listFriends}/>
                                }
                            </div>
                        </>
                    }
                >
                    <SimpleFooter/>
                    <FeedBackAlert/>
                </SideBar>
            </>
        );
    }
}

export default Abo;
