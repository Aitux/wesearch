/*!

=========================================================
* Argon Design System React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-design-system-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-design-system-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import ReactDOM from "react-dom";
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";

import "assets/vendor/nucleo/css/nucleo.css";
import "assets/vendor/font-awesome/css/font-awesome.min.css";
import "assets/scss/argon-design-system-react.scss";
import Login from "views/examples/Login.jsx";
import Profile from "views/examples/Profile.jsx";
import Register from "views/examples/Register.jsx";
import PasswordForgotten from "./views/examples/PasswordForgotten";
import RegisterProject from "./views/examples/RegisterProject";
import Project from "./views/examples/Project";
import Welcome from "./views/examples/Welcome";
import Logout from "./views/examples/Logout";
import ModificationProfile from "./views/examples/ModificationProfile";
import ModificationProject from "./views/examples/ModificationProject";
import Search from "./views/examples/Search";
import Abo from "./views/examples/Abo";
import ListProjects from "./views/examples/ListProjects";


ReactDOM.render(
    <BrowserRouter>
        <Switch>
            <Route
                path="/"
                exact
                render={props => <Login {...props} />}/>
            <Route
                path="/passwordForgotten"
                exact
                render={props => <PasswordForgotten {...props} />}
            />
            <Route
                path="/welcome"
                exact
                render={props => <Welcome {...props} />}
            />
            <Route
                path="/listproject"
                exact
                render={props => <ListProjects {...props} />}
            />

            <Route
                path="/project"
                exact
                render={props => <Project {...props} />}
            />
            <Route path="/login"
                   exact
                   render={props => <Login {...props} />}/>
            <Route
                path="/profile"
                exact
                render={props => <Profile {...props} />}
            />
            <Route
                path="/register"
                exact
                render={props => <Register {...props} />}
            />
            <Route
                path="/logout"
                exact
                render={props => <Logout {...props} />}
            />

            <Route
                path="/registerProject"
                exact
                render={props => <RegisterProject {...props} />}
            />

            <Route
                path="/modificationProfile"
                exact
                render={props => <ModificationProfile {...props} />}
            />

            <Route
                path="/modificationProject"
                exact
                render={props => <ModificationProject {...props} />}
            />

            <Route
                path="/search"
                exact
                render={props => <Search {...props} />}
            />
            <Route
                path="/abo"
                exact
                render={props => <Abo {...props} />}
            />
            <Route
                path="/listProjects"
                exact
                render={props => <ListProjects {...props} />}
            />
            <Redirect to="/"/>
        </Switch>
    </BrowserRouter>,
    document.getElementById("root")
);
