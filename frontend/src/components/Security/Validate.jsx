import React from 'react';
import RequestPage from "./RequestPage";
import {Redirect} from "react-router";

const Validate = () => {
    function isLoggedIn() {
        return RequestPage(1);
    }

    if (!isLoggedIn()) {
        return (
            <Redirect to="/login"/>
        )
    } else {
        return null;
    }
};

export default Validate;