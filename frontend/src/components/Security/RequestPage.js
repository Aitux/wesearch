const RequestPage = async (roleAuth) => {
    let tok = localStorage.getItem('token_wesearch');

    if (tok === null)
        tok = '0';
    let res = false;
    const anon = {
        'token': tok,
        'allowed': roleAuth
    };
    if (tok === undefined) return false;
    const response = await fetch('https://wesearchgroup-preprod.herokuapp.com/validate', {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(anon)
    });
    const data = await response.json();
    res = await data['response'];

    if (res === true)
        return true;
};

export default RequestPage;