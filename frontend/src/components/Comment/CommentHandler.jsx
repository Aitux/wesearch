import React from 'react';
import {Row} from "reactstrap";
import UseAnimations from 'react-useanimations';
import AtomicComment from "./AtomicComment";

const CommentHandler = (props) => {
    const {listComment, towo} = props;
    console.log(listComment)
    return (
        <>
            {

                (listComment != null && listComment.length > 0) ? listComment.slice(0, towo != null ? towo : listComment.length).map(comment =>
                        (
                            <AtomicComment key={comment.idcomment} content={comment.content} date={comment.date}
                                           id={comment.users.idUsers} prenom={comment.users.prenom}/>
                        )) :

                    <>
                        <Row>
                            <div className="m-auto"><UseAnimations animationKey="airplay" className="" size={22}/></div>
                        </Row>
                        <Row><p className="m-auto"> Pas de commentaires... Soit le premier à commenter ceci !</p></Row>
                    </>

            }
        </>
    );

};

export default CommentHandler;