import React from "react";
import {Button, Card, CardBody, Col, Form, FormGroup, Input, Row} from "reactstrap";

class CommentPost extends React.Component {

    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.addPost = this.addPost.bind(this);
        this.state = {
            pcontenu: '',
        }

    }


    addPost() {
        const {pcontenu} = this.state;
        console.log("Post ajouté !");
        const bodied = JSON.stringify({
            idUsers: parseInt(localStorage.getItem('iduser_wesearch')),
            content: pcontenu,
            date: Date.now(),
            idcommentable: this.props.idpost
        });
        console.log(bodied);
        const URL = 'https://wesearchgroup-preprod.herokuapp.com/comment';
        return fetch(URL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: bodied
        })
            .then(response => response.json())
            .then(data => {
                window.location.reload();
            });
    }

    render() {
        const {pcontenu} = this.state;
        return (
            <>
                <Card>
                    <CardBody>
                        <Form>
                            <FormGroup>
                                <Row>
                                    <Col sm={10}>
                                        <Input
                                            type="text"
                                            name="pcontenu"
                                            placeholder="Quelque chose à ajouter ?"
                                            value={pcontenu}
                                            onChange={this.handleChange}
                                        />
                                    </Col>
                                    <Col>
                                        <Button color="primary"
                                                onClick={this.addPost}
                                                outline={true}
                                        >
                                            Poster
                                        </Button>
                                    </Col>
                                </Row>
                            </FormGroup>

                        </Form>
                    </CardBody>
                </Card>
            </>);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

}

export default CommentPost;