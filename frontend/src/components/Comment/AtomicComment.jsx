import React from 'react';
import "../../assets/css/postfeed.css"
import '../../assets/css/postposter.css'
import {Card, CardBody, CardText} from "reactstrap";
import {Link} from "react-router-dom";


class AtomicComment extends React.Component {

    profile = `/profile?q=${this.props.id}`;


    timeSince = (date) => {
        let seconds = Math.floor((new Date() - date) / 1000);
        let interval = Math.floor(seconds / 31536000);

        if (interval > 1) {
            return interval + " years ago";
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
            return interval + " months ago";
        }
        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
            return interval + " days ago";
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
            return interval + " hours ago";
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
            return interval + " minutes ago";
        }
        return Math.floor(seconds) + " seconds ago";
    };

    render() {
        return (
            <>
                <Card className="post_container" id={this.props.idcomment}>
                    <CardBody>
                        <CardText className="post_header">
                            <Link to={this.profile}>{this.props.prenom}</Link> - <span
                            className="post_hour">{this.timeSince(new Date(this.props.date))}</span>
                        </CardText>
                        <CardText className="post_text" dangerouslySetInnerHTML={{__html: this.props.content}}/>
                    </CardBody>
                </Card>
            </>
        );
    }
}

export default AtomicComment;