import React from 'react';
import {UncontrolledAlert} from "reactstrap";
import '../../assets/css/alert.css';

const FeedBackAlert = (props) => {
    return (
        <UncontrolledAlert className="alert-default sah" fade={false}>
          <span className="alert-inner--icon">
            <i className="ni ni-like-2"/>
          </span>{" "}
            <span className="alert-inner--text">
            Si vous souhaitez nous faire part d'un bug ou nous faire un retour pour nous aider à faire progresser l'application n'hésitez pas à <strong>cliquer ici!</strong>
          </span>
        </UncontrolledAlert>);
};

export default FeedBackAlert;