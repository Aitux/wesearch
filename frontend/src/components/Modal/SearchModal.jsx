import React, {useState} from 'react';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader, UncontrolledTooltip} from 'reactstrap';

const SearchModal = (props) => {
	const {buttonLabel, className, title, textToolTip} = props;

	const [modal, setModal] = useState(false);
	const toggle = () => setModal(!modal);

	return (
		<>
			<nav>
				<Button
					onClick={toggle}
					className="icon icon-shape bg-white rounded-circle text-primary"
					id="tooltip23101997">
					{buttonLabel}
				</Button>
				<UncontrolledTooltip delay={0} target="tooltip23101997">
					{textToolTip}
				</UncontrolledTooltip>
				<Modal isOpen={modal} toggle={toggle} className={className}>
					<ModalHeader toggle={toggle}>{title}</ModalHeader>
					<ModalBody>

					</ModalBody>
					<ModalFooter>
						<Button color="primary" onClick={toggle}>Recherche</Button>{' '}
						<Button color="secondary" onClick={toggle}>Annuler</Button>
					</ModalFooter>
				</Modal>
			</nav>

		</>
	)
};

export default SearchModal;