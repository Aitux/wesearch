import React from 'react';

import {Row} from "reactstrap";
import AtomicProject from "../Project/AtomicProject";
import AtomicUser from "./AtomicUser";
import Container from "reactstrap/es/Container";


const SearchHandler = (props) => {
    const {projects, users, towo} = props;
    console.log(projects);
    console.log(users);
    return (
        <>
            <Container>
                {

                    (projects != null && projects.slice(0, towo != null ? towo : projects.size).map((project) => (
                        <Row><AtomicProject title={project.title} startingDate={project.startingdate}
                                            description={project.description} id={project.idproject}
                                            key={project.idcommentable}/></Row>)))
                }
                {
                    (users != null && users.slice(0, towo != null ? towo : users.size).map((users) => (
                        <Row><AtomicUser nom={users.nom} prenom={users.prenom} id={users.idUsers} job={users.job}
                                         domain={users.domains} key={users.idUsers}/></Row>)))
                }
            </Container>

        </>
    );

};

export default SearchHandler;