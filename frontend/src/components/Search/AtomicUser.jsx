import React from 'react';
import {Badge, Button, Card, CardText, CardTitle} from "reactstrap";
import "../../assets/css/atomicsearch.css"
import {Link} from "react-router-dom";
import HandlerDomain from "../Domain/HandlerDomain";

const AtomicUser = (props) => {
    const {nom, prenom, id, job, domain} = props;
    const profile = `/profile?q=${id}`;

    return (
        <Card body className="atom_card d-flex flex-column ">
            <CardTitle className="d-flex"><Link to={profile}>{prenom} {nom}</Link> <Badge className="ml-auto" color="success"
                                                                pill>Utilisateur</Badge><Badge color="primary"
                                                                                               pill>{job}</Badge></CardTitle>
            <CardText><HandlerDomain listDomains={domain}/></CardText>
            <Link to={profile} className="mt-auto ml-auto"> <Button color="primary" className="mt-auto">Voir profil</Button></Link>
        </Card>

    )

};

export default AtomicUser;