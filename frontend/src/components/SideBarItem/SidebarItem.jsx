import React from "react";
import {Link} from "react-router-dom";


const SidebarItem = (props) => {
    const {icon, link, text} = props;
    return (
        <Link
            className="d-flex align-items-center"
            to={link}
        >
            <div
                className="icon icon-shape just">
                {icon}
            </div>
                <h6 className="text-primary mb-md-1">
                    {text}
                </h6>

        </Link>
    );
};

export default SidebarItem;