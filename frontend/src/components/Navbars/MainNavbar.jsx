/*!
=========================================================
* Argon Design System React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-design-system-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-design-system-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/
import React from 'react';
import {Link} from 'react-router-dom';
import {Button, Col, Container, Input, Row} from 'reactstrap';

class MainNavbar extends React.Component {


    constructor() {
        super();
        this.state = {
            search: ""
        }
    }

    componentDidMount() {
    }

    render() {
        let query = `/search?q=${this.state.search}`;
        return (
            <>
                <Container
                    style={{marginTop: "2rem"}}
                >
                    <Row>
                        <Col>
                            <img
                                alt="..."
                                src={require('assets/img/brand/logo_transparent.png')}
                                width={"200px"}
                            />
                        </Col>
                        <Col lg="4">
                            <Input
                                className="form-control-alternative"
                                id="text"
                                name="search"
                                placeholder="Tapez ici"
                                onChange={this.handleChange}
                                type="text"
                            />
                        </Col>
                        <Col>

                            <Link
                                to={query}
                            >
                                <Button id="addPro"
                                        className="btn btn-primary"
                                        color="alert"
                                >
                                    Rechercher
                                </Button>
                            </Link>
                        </Col>

                        <Link
                            onClick={this.logout}
                            to={"/logout"}
                            style={{paddingTop: "1%"}}
                        >
                            <i className="ni ni-button-power"/>
                        </Link>

                    </Row>
                </Container>
            </>
        );
    }

    logout = event => {
        fetch("https://wesearchgroup-preprod.herokuapp.com/logout/" + localStorage.getItem("token_wesearch"), {
            method: 'GET',
        })
    };

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

}

export default MainNavbar;
