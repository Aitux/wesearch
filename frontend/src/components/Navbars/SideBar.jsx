import React from 'react';
import {Col, Container, Row} from 'reactstrap';
import SidebarItem from "../SideBarItem/SidebarItem";
import '../../assets/css/sidebar.css';
import {Link} from "react-router-dom";
import queryString from "query-string";

class SideBar extends React.Component {

    constructor() {
        super();
        this.state = {
            usersConnected:"",

        };
    }

    componentDidMount() {
        fetch(`https://wesearchgroup-preprod.herokuapp.com/connectedUser`)
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({
                    usersConnected: data.response
                });
            })
    };

    render() {
        const urlProfil = `/profile?q=${localStorage.getItem('iduser_wesearch')}`;

        return (
            <Row className="theSide">
                <Col lg={2}>
                    <Container>
                        <nav className="sidebar">
                            <ul>
                                <Row>
                                    <Col>
                                        <li>
                                            <SidebarItem icon={<i className="ni ni-world"/>} text="Accueil"
                                                         link="/welcome"/>
                                        </li>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <li>
                                            <SidebarItem icon={<i className="ni ni-spaceship"/>} text="Abonnements"
                                                         link="/abo"/>
                                        </li>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>

                                        <li>
                                            <SidebarItem icon={<i className="ni ni-atom"/>} text="Projets"
                                                         link="/listProjects"/>
                                        </li>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <li>
                                            <SidebarItem icon={<i className="ni ni-circle-08"/>}
                                                         text={"Profil"}
                                                         link={urlProfil}
                                            />
                                        </li>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <li>

                                            <Link id="addPro"
                                                  className="btn btn-primary"
                                                  color="alert"
                                                  to={"/registerProject"}>
                                                Créer un projet
                                            </Link>
                                        </li>
                                    </Col>
                                </Row>
                                <br/>
                                <br/>
                                <br/>
                                <Row>
                                    <Col>

                                            <div>
                                                Utilisateurs connectés :  &nbsp;
                                                {this.state.usersConnected}
                                            </div>

                                    </Col>
                                </Row>
                            </ul>
                        </nav>
                    </Container>
                </Col>
                <Col>
                    {this.props.center}
                </Col>

            </Row>
        )


    };
}

export default SideBar;