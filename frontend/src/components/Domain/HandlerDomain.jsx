import React from 'react';
import AtomicDomain from "./AtomicDomain";


const HandlerDomain = (props) => {
    const {listDomains, towo} = props;
    console.log(listDomains);
    return (
        <>
            {

                (listDomains != null && listDomains.length > 0) ? listDomains.slice(0, towo != null ? towo : listDomains.length)
                    .map(domain => <AtomicDomain label={domain.libelle}/>) : console.log('nothing to print in domains')
            }
        </>
    );

};

export default HandlerDomain;