import React from 'react';
import {Badge} from "reactstrap";

const AtomicHandler = (props) => {
    const {label} = props;

    function randomDeterministic() {
        const alphanum = {
            'a': 1,
            'b': 2,
            'c': 3,
            'd': 4,
            'e': 5,
            'f': 6,
            'g': 7,
            'h': 8,
            'i': 9,
            'j': 10,
            'k': 11,
            'l': 12,
            'm': 13,
            'n': 14,
            'o': 15,
            'p': 16,
            'q': 17,
            'r': 18,
            's': 26,
            't': 19,
            'u': 20,
            'v': 21,
            'w': 22,
            'x': 23,
            'y': 24,
            'z': 25
        };
        let res = 0;
        const tmplabel = label.toLowerCase().replace(/[^a-zA-Z]/g, "");
        for (let i = 0; i < tmplabel.length; i++) {
            const letter = tmplabel.charAt(i);
            res += alphanum[letter];
        }

        res = res % 8;
        const poss = ['primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark'];
        return poss[res];
    }

    return (<Badge color={randomDeterministic()} pill>{label}</Badge>);
};

export default AtomicHandler;