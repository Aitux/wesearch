import React from 'react';
import AtomicProject from "./AtomicProject";
import {Col, Row} from "reactstrap";


const ProjectHandler = (props) => {
    const {listProjects, towo, lg} = props;
    return (
        <>
            <Row>
                {

                    (listProjects != null && listProjects.slice(0, towo != null ? towo : listProjects.size).map((project) => (
                        <Col lg={lg || 4}><AtomicProject title={project.title} startingDate={project.startingdate}
                                                   description={project.description} id={project.idproject}
                                                   key={project.idcommentable}/></Col>)))
                }
            </Row>

        </>
    );

};

export default ProjectHandler;