import React from 'react';
import {Button, Card, CardText, CardTitle} from "reactstrap";
import "../../assets/css/atomicsearch.css"
import {Link} from "react-router-dom";

const AtomicProject = (props) => {
    const {title, startingDate, description, id} = props;
    const project = `/project?q=${id}`;

    function timeSince(date) {
        let seconds = Math.floor((new Date() - date) / 1000);
        let interval = Math.floor(seconds / 31536000);

        if (interval > 1) {
            return interval + " years ago";
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
            return interval + " months ago";
        }
        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
            return interval + " days ago";
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
            return interval + " hours ago";
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
            return interval + " minutes ago";
        }
        return Math.floor(seconds) + " seconds ago";
    }

    function truncContent(content, nb) {
        if (content.length > nb)
            return content.substr(0, nb) + ' ...';
        else
            return content
    }

    return (

        // eslint-disable-next-line react/jsx-no-undef
        <Card body className="atom_card d-flex flex-column">
            <CardTitle><Link to={project}>{truncContent(title, 24)}</Link><div className="text-muted tamere text-uppercase">{timeSince(new Date(startingDate))}</div></CardTitle>
            <CardText>{truncContent(description, 110)}</CardText>
            <Link to={project} className="mt-auto ml-auto"> <Button color="primary" className="mt-auto">Voir plus</Button></Link>
        </Card>

    )

};

export default AtomicProject;