import React from "react";
// reactstrap components
import {Col, Container, Nav, NavItem, Row} from "reactstrap";

class SimpleFooter extends React.Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    //   let prefix = window.location.host.startsWith("http") ? "" : "http://";
    //   //using host here, as I'm redirecting to another location on the same host
    //   this.target = "https://docs.google.com/forms/d/1LPq6Sr44KVDAwUxuZaYziwFC9y6ac2D9O5qNvY6CEbk/edit";
  }


  render() {
    return (
        <>
          <div><br/></div>
          <footer className="footer">
            <Container>

              <Row className="align-items-center justify-content-md-between">
                <Col md="6">
                  <div className=" copyright">
                    © {new Date().getFullYear()}{" "}
                    <a
                        href="https://www.creative-tim.com?ref=adsr-footer"
                        // target="_blank"
                    >
                      WeSearchGroup
                    </a>
                    .
                  </div>
                </Col>
                <Col lg="6">
                  <Nav className=" nav-footer justify-content-end">
                    <NavItem>
                      <a
                          href="https://docs.google.com/forms/d/1LPq6Sr44KVDAwUxuZaYziwFC9y6ac2D9O5qNvY6CEbk/edit"
                          // target="_blank"
                          onClick={this.handleClick}
                      >
                        Questionnaire de satisfaction
                      </a>
                    </NavItem>
                  </Nav>
                </Col>
              </Row>
            </Container>
          </footer>
        </>
    );
  }

  handleClick() {
    window.location.replace('https://docs.google.com/forms/d/1LPq6Sr44KVDAwUxuZaYziwFC9y6ac2D9O5qNvY6CEbk/edit')
  }

}

export default SimpleFooter;
