import React from 'react';
import {Button} from "reactstrap";


const FollowerHandler = (props) => {
    const {list, towo, handler} = props;
    return (
        <>
            {

                (list != null && list.slice(0, towo != null ? towo : list.size).map(item =>
                    <>
                        <li>{item.label}
                        <i className="ni ni-fat-remove" onClick={e=> handler(item)}/></li>
                    </>))
            }
        </>
    );

};

//

export default FollowerHandler;