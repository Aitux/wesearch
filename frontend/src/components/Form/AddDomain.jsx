import React from "react";
import '../../assets/css/postposter.css'
import Select from 'react-select';
import '../../assets/css/select-search.css'
import {Button} from "reactstrap";

class AddDomain extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            options: [],
            selectedOption: null
        }
    }

    componentDidMount() {

        fetch('https://wesearchgroup-preprod.herokuapp.com/domain').then(response => response.json()).then(data => {
            let res = [];
            data.slice().map(item => {
                res.push({
                    label: item.libelle,
                    value: item.iddomain
                });
            });
            this.setState({options: res});
        });
    }

    addDomain = e => {
        const {selectedOption} = this.state;
        !this.props.unique ?
            fetch(`https://wesearchgroup-preprod.herokuapp.com/${this.props.to}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idUsers: localStorage.getItem("iduser_wesearch"),
                iddomain: selectedOption.value
            })
        }).then(response => response.json()) : fetch(`https://wesearchgroup-preprod.herokuapp.com/${this.props.to}${selectedOption.value}`)
    };

    handleChange = selectedOption => {
        this.setState(
            {selectedOption},
            () => console.log(`Option selected:`, this.state.selectedOption)
        );
    };


    render() {
        const {selectedOption} = this.state;
        return (
            <>
                <Select
                    value={selectedOption}
                    onChange={this.handleChange}
                    closeMenuOnSelect={true}
                    options={this.state.options}/>
                <Button color="primary"
                        onClick={this.addDomain}
                >
                    OK
                </Button>
            </>
        );
    }
}

export default AddDomain;