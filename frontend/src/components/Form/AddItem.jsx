import React from "react";
import '../../assets/css/postposter.css'
import {Input} from "reactstrap";

class AddItem extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            type: false
        }
    }

    render() {
        return (
            <>
                {this.props.iduser === localStorage.getItem('iduser_wesearch') ? <></> : !this.state.type ?
                    <span className='text-muted text-left damn' onClick={this.focusInput}> <i
                        className="ni ni-fat-add"/>Ajouter élément</span> :
                    <Input autoFocus onKeyPress={this.handleKeyPress}
                           placeholder={'Appuyez sur Entrée quand vous avez fini !'}
                           type='text'
                           onBlur={this.blurComponent}/>}
            </>
        );
    }

    handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            fetch(`https://wesearchgroup-preprod.herokuapp.com/${this.props.to}`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    idproject: this.props.id,
                    label: event.target.value
                })

            })
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    this.setState({type: false});
                    this.props.handler(data);
                });
        }
    };

    deleteItem = (id, to) => {
        fetch(`https://wesearchgroup-preprod.herokuapp.com/${to}/${id}`, {
            method: 'DELETE',
        })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({type: false});
                this.props.handleDelete(id);
            });
    };

    focusInput = event => {
        this.setState({type: true});
    };

    blurComponent = event => {
        this.setState({type: false});
    }
}

export default AddItem;