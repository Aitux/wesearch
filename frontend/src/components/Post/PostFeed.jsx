import React from 'react';
import {Card, CardBody, CardFooter, CardHeader, CardText, Container, Row} from "reactstrap";
import "../../assets/css/postfeed.css"
import {Link} from "react-router-dom";
import '../../assets/css/postposter.css'
import CommentPost from "../Comment/CommentPost";
import CommentHandler from "../Comment/CommentHandler";


class PostFeed extends React.Component {

    profile = `/profile?q=${this.props.id}`;

    constructor(props) {
        super(props);
        this.state = {
            shouldHide: true,
            listComment: [],
            readOnly : this.props.readOnly || false,
        };
    }

    componentDidMount() {
        setTimeout(() => {
            fetch(`https://wesearchgroup-preprod.herokuapp.com/comment/${this.props.idpost}`).then(response => response.json()).then(data => {
                this.setState({listComment: data});
                console.log(this.state.listComment);
            });
        }, 1000);
    }

    timeSince = (date) => {
        let seconds = Math.floor((new Date() - date) / 1000);
        let interval = Math.floor(seconds / 31536000);

        if (interval > 1) {
            return interval + " years ago";
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
            return interval + " months ago";
        }
        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
            return interval + " days ago";
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
            return interval + " hours ago";
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
            return interval + " minutes ago";
        }
        return Math.floor(seconds) + " seconds ago";
    };

    removePost = (e) => {
        fetch(`https://wesearchgroup-preprod.herokuapp.com/post/${this.props.idpost}`, {
            method: 'DELETE'
        }).then(_ => window.location.reload());

    };

    swapDisplay = () => {
        this.setState({shouldHide: !this.state.shouldHide});
    };

    render() {
        return (
            <Card className="post_container" id={this.props.idpost}>
                <CardHeader className="post_header">
                    <Container>
                        <Row className="d-flex h-100">
                            <Link className="align-self-end" to={this.profile}>{this.props.prenom}</Link>
                            <span
                                className="post_hour align-self-end ml-2">{`@${this.props.prenom}${this.props.nom}`}</span>
                            <span
                            className="post_hour align-self-end ml-2">{this.timeSince(new Date(this.props.date))}</span>
                            <Link
                                onClick={(e) => this.removePost(e)}
                                className={localStorage.getItem("iduser_wesearch") == this.props.id ? 'ml-auto' : 'hidden ml-auto'}
                                to={'/welcome'}
                            >
                                <i className="ni ni-fat-remove"/>
                            </Link>
                        </Row>
                    </Container>
                </CardHeader>
                <CardBody>
                    <CardText className="ml-0" dangerouslySetInnerHTML={{__html: this.props.content}}/>
                </CardBody>
                <Container>
                    <div className="d-flex"><p className="ml-auto mb-1 mr-1 text-underline comment"
                                                   onClick={this.swapDisplay}>{`Commentaire (${this.state.listComment.length})`}</p></div>
                </Container>
                <CardFooter className={this.state.shouldHide ? "hidden" : ''}>
                    {
                        this.state.shouldHide ?
                            <></> : <>
                            {this.state.readOnly ? <></> : <CommentPost idpost={this.props.idpost} />}
                                <CommentHandler listComment={this.state.listComment}/>
                            </>
                    }

                </CardFooter>
            </Card>)
    }


}

export default PostFeed;

