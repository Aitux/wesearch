import React from "react";
import {Button, Card, CardBody, Form, FormGroup, Input} from "reactstrap";

class PostPoster extends React.Component {

    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.addPost = this.addPost.bind(this);
        this.displayBut = this.displayBut.bind(this);
        this.hideBut = this.hideBut.bind(this);
        this.state = {
            pcontenu: '',
            displayBut : false,
        }

    }

    displayBut(event){
        this.setState({displayBut: true});

    }

    hideBut(event){
        setTimeout(() => {
            this.setState({displayBut: false});
        }, 100);

    }

    addPost() {
        const {pcontenu} = this.state;
        console.log("Post ajouté !");
        const bodied = JSON.stringify({
            idusers: parseInt(localStorage.getItem('iduser_wesearch')),
            content: pcontenu,
            datepost: Date.now()
        });
        console.log(bodied);
        const URL = 'https://wesearchgroup-preprod.herokuapp.com/post';
        return fetch(URL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: bodied
        })
            .then(response => response.json())
            .then(data => {
                console.log(data.status);
                console.log(data);
                window.location.reload();
            });
    }

    render() {
        const {pcontenu} = this.state;
        return (
            <>
                <Card>
                    <CardBody>
                        <Form>
                            <FormGroup>
                                <Input
                                    type="text"
                                    name="pcontenu"
                                    id="post_content"
                                    placeholder="Quoi de neuf ?"
                                    value={pcontenu}
                                    onChange={this.handleChange}
                                    onFocus={this.displayBut}
                                    onBlur={this.hideBut}
                                />
                            </FormGroup>
                            <div className="d-flex">
                                <Button color="primary"
                                        onClick={this.addPost}
                                        outline={true}
                                        className={this.state.displayBut ? 'ml-auto' : 'ml-auto hidden'}
                                >
                                    Poster
                                </Button>
                            </div>
                        </Form>
                    </CardBody>
                </Card>
            </>);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

}

export default PostPoster;