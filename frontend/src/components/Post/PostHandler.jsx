import React from 'react';
import PostFeed from './PostFeed';
import {Row} from "reactstrap";
import UseAnimations from 'react-useanimations';

class PostHandler extends React.Component {
    render() {
        const {listPost, towo, readOnly} = this.props;

        return (
            <>
                {

                    (listPost != null && listPost.length > 0) ? listPost.slice(0, towo != null ? towo : listPost.length).map(post =>
                            (
                                <PostFeed prenom={post.users.prenom} nom={post.users.nom} date={post.date}
                                          content={post.content}
                                          id={post.users.idUsers} key={post.idPost} idpost={post.idPost} shouldHide={true}
                                          readOnly={readOnly}/>
                            )) :

                        <>
                            <Row>
                                <UseAnimations animationKey="infinity" className="m-auto" size={56}/>
                            </Row>
                        </>

                }
            </>
        );
    }
}
;

export default PostHandler;