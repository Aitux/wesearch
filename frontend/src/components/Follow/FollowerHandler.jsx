import React from 'react';
import {Col, Row} from "reactstrap";
import AtomicFollower from "./AtomicFollower";


const FollowerHandler = (props) => {
    const {listProfile, towo} = props;
    console.log(listProfile && listProfile);
    return (
        <>
            <Row>
                {

                    (listProfile != null && listProfile.slice(0, towo != null ? towo : listProfile.size).map((profile) => (
                        <Col lg={3}><AtomicFollower nom={profile.nom} prenom={profile.prenom}
                                                    email={profile.mail} id={profile.idUsers}
                                                    key={profile.idUsers} job={profile.job}/></Col>)))
                }
            </Row>

        </>
    );

};

export default FollowerHandler;