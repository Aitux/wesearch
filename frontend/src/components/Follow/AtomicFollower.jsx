import React from 'react';
import {Button, Card, CardBody, CardSubtitle, CardText, CardTitle} from "reactstrap";
import "../../assets/css/atomicproject.css"
import {Link} from "react-router-dom";

const AtomicFollower = (props) => {
    const {email, prenom, nom, id, job} = props;
    const profil = `/profile?q=${id}`;


    return (

        // eslint-disable-next-line react/jsx-no-undef
        <Card>
            {/*<CardImg top width="100%" src="" alt="Profile pic"/>*/}
            <CardBody>
                <CardTitle>{nom} {prenom}</CardTitle>
                <CardSubtitle>{job}</CardSubtitle>
                <CardText>{email}</CardText>
                <Link to={profil}><Button>Voir profil</Button></Link>
            </CardBody>
        </Card>

    )

};

export default AtomicFollower;